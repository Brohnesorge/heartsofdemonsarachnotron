class ArachnoBaseWeapon : Weapon
{
	string PsyReady; //what power is readied, for the hud
	double PsyCharge; //Focus. What counts up before power can be released
	int PsyCost; //PP cost
	int PsyCooldown; //Cooldown amount added
	int FocusTarget; //What Focus needs to count up to before a successful cast
	int BloodCost; //Amount of damage taking from not enough PP
	bool BrainCheck; //Did Brainwash hit something? Used for the refund check.
	
	string SubReady; //what subweapon is readied, for hud
	int SubCharge; //what counts up before its armed
	int SubArm; //the target amount for SubCharge, universal
	int SubCost; //The amount of Sub Ammo used
	int SubAmt; //what counts the amount of shots 
	int SubTarget; //what SubAmt counts up to; total number of shots
	bool SubSide; //what side shit fires from. True is left, false is right.
	bool SubFiring; //if the weapon is firing or not, for the hud
	int SubDelay; //Delay between shots
	bool SataeSafe; //Check ceiling for firing Satae.
	bool ScorpUp; // Scorpion is charging, for the HUD
	int ScorpCharge; //Scorpion's charge up, after the arming
	int ScorpTarget; //What the Scorpion needs to charge up to

	string shortTag;
	property ShortTag: shortTag;

	enum PsionicPowers
	{
		isForce 	= 0,
		isRenew		= 1,
		isPossess   = 2
	}
	
	enum Subweapons
	{
		isGrenades 	= 0,
		isRockets	= 1,
		isLaser		= 2
	}
	
	private action state A_ListenAttackKeys(int overlayId)
	{
		let plr = TheArachnotron(self);
		if (player.cmd.buttons & BT_USER1)
		{	
			return ResolveState("SubWeaponFire");
		}
		
		return ResolveState(null);
	}
	
	action void A_PsiForce()
	{
		A_FireProjectile("MindForceShot", 0, FALSE);
		A_QuakeEX(invoker.PsyCharge * 0.01, invoker.PsyCharge * 0.01, invoker.PsyCharge * 0.01, 25, 0, 128, flags: QF_SCALEDOWN);
		
		if(invoker.PsyCharge > 245)
		{
			A_Recoil(8);
		}
	}
	
	action void A_PsiRenew()
	{
		A_KillChildren(species: "TheArachnotron");
		if(CheckInventory("MegaResist", 1))
		{
			A_SpawnItemEx("QolMegaBarrier", flags: SXF_SETMASTER); 
			A_TakeInventory("MegaResist", 1);
		}
		else A_SpawnItemEx("QolBarrier", flags: SXF_SETMASTER); 
	}
	
	action void A_PsiBrainwash()
	{
		let plr = TheArachnotron(invoker.owner);
		
		FLineTraceData Submissive;
		bool checker = plr.LineTrace(plr.Angle, 360000, plr.Pitch, TRF_THRUSPECIES, player.viewz - player.mo.pos.z,  data: Submissive);
		
		if(Checker)
		{	
			if(checker && Submissive.HitActor)
			{
				if(Submissive.HitActor.bISMONSTER)
				{
					if(Submissive.HitActor.SpawnHealth() <= 500 && Submissive.HitActor.bBOSS == FALSE)
					{
						Submissive.HitActor.bFRIENDLY = TRUE;
						Submissive.HitActor.bNOPAIN	= TRUE;
						Submissive.HitActor.bMISSILEEVENMORE = TRUE;
						Submissive.HitActor.A_SetRenderstyle(0.75, STYLE_ADD);
					
						A_Print(Submissive.HitActor.GetTag().." has been brainwashed");
						invoker.PsyCooldown = invoker.PsyCooldown + (Submissive.HitActor.SpawnHealth() * 0.5);
					}
					else 
					{
						if(!random(0, Submissive.HitActor.SpawnHealth() * 0.005))
						{
							if(Submissive.HitActor.bBOSS == FALSE || !random(0, 1) & Submissive.HitActor.bBOSS == TRUE)
							{
								Submissive.HitActor.bFRIENDLY = TRUE;
								Submissive.HitActor.bNOPAIN	= TRUE;
								Submissive.HitActor.bMISSILEEVENMORE = TRUE;
								Submissive.HitActor.A_SetRenderstyle(0.75, STYLE_ADD);
							
								A_Print(Submissive.HitActor.GetTag().." has been brainwashed");
								invoker.PsyCooldown = invoker.PsyCooldown + (Submissive.HitActor.SpawnHealth() * 0.66);
							}
						}
						else 
						{
							A_Print(Submissive.HitActor.GetTag().." resisted brainwashing\nTarget too strong");
							invoker.PsyCooldown = 1240;
						}
					}
				}
				else 
				{
					invoker.BrainCheck = TRUE;
					invoker.PsyCost = 0;
					invoker.PsyCooldown = 0;
				}
			}
		}
	}
	
	action void A_PsiOtherStuff()
	{
		let plr = TheArachnotron(self);
	
		A_StartSound("MINDBLAS", CHAN_WEAPON);
		A_SetBlend("99 00 99", 0.25, 15);
		A_AlertMonsters();
		A_ZoomFactor(1.0);
		
		invoker.PsyCharge = 0;
		
		if(!invoker.BrainCheck)
		{
			if(plr.CooldownTime) A_DamageSelf(invoker.PsyCost * 0.5, flags: DMSS_NOPROTECT | DMSS_NOFACTOR);
			
			if(CheckInventory("HODPP", invoker.PsyCost)) A_TakeInventory("HODPP", invoker.PsyCost);
			else 
			{
				invoker.BloodCost = invoker.PsyCost - CountInv("HODPP");
				A_TakeInventory("HODPP");
				A_DamageSelf(invoker.BloodCost, flags: DMSS_NOPROTECT | DMSS_NOFACTOR);
			}
		}
		
		plr.CooldownTime = plr.CooldownTime + invoker.PsyCooldown;
	}
	
	action void A_SubGrenande()
	{
		A_FireProjectile("Prosoma", 0, FALSE, invoker.SubSide ? 18 : -18, 0, 0, -15);
		A_StartSound("Weapons/Prosoma", 98);
		
		invoker.SubSide = invoker.SubSide ? FALSE : TRUE;
	}
	
	action void A_SubRockets()
	{
		A_FireProjectile("Satae", 0, FALSE, invoker.SubSide ? 18 : -18, pitch: -90);
		A_StartSound("Satae/Launch", 98);
		
		invoker.SubSide = invoker.SubSide ? FALSE : TRUE;
	}
	
	action void A_SubLaser()
	{
		A_StartSound("SCORFIRE", 88);
		A_RailAttack(0, 0, FALSE, "", "", RGF_SILENT, 0, "ScorpPuff", 0, 0, 0 , 0, 1.0, 0, "ScorpLaserBall", 24);
	}
	
	action void A_SubOtherStuff()
	{
		let plr = TheArachnotron(self);
		
		A_SetTics(invoker.SubDelay);
		A_AlertMonsters();
		
		invoker.SubCharge = 0;
		invoker.SubAmt --;
	}
	
	override void Tick()
	{
		let plr = TheArachnotron(self.Owner);
	
		switch(plr.PowerReady)
		{
			case isForce : 
				PsyReady = "FORCE";
				FocusTarget = 3;	
				
				if(PsyCharge > 245)
				{	
					PsyCost = 150; 	
					PsyCooldown = 1050;	
				}				
				else if(PsyCharge > 105)
				{	
					PsyCost = 35; 	
					PsyCooldown = 350;	
				}
				else if(PsyCharge > 60)
				{	
					PsyCost = 15; 	
					PsyCooldown = 105;	
				}
				else if(PsyCharge > 20)
				{	
					PsyCost = 10; 	
					PsyCooldown = 17;	
				}
				else if(PsyCharge < 20)
				{	
					PsyCost = 5; 	
					PsyCooldown = 2;	
				}
				break;
			case isRenew : 
				PsyReady = "RENEW";
				FocusTarget = 105;
				PsyCost = 25;
				PsyCooldown = 350;
				break;
			case isPossess : 
				PsyReady = "BRAINWASH";
				FocusTarget = 140;
				PsyCost = 100;
				PsyCooldown = 245;
				break;
		}
		
		switch(plr.SubReady)
		{
			case isGrenades:
				SubReady = "PROSOMA";
				SubCost	= 1;
				SubTarget = 8;
				SubDelay = 7;
				break;
			case isRockets:
				SubReady = "SATAE";
				SubCost = 5;
				SubTarget = 16;
				SubDelay = 3;
				break;
			case isLaser:
				SubReady = "SCORPION";
				SubCost = 25;
				SubTarget = 60;
				SubDelay = 1;
				break;		
		}
	
		super.Tick();
	}
	
	override void PostBeginPlay()
	{
		SubArm = 3;
		ScorpCharge = 0;
		ScorpTarget = 75;
	
		super.PostBeginPlay();
	}
	
	Default
	{
		Weapon.BobSpeed 0;
		Weapon.BobRangeX 0.0;
		Weapon.BobRangeY 0.0;
		
		+WEAPON.NODEATHINPUT;
		+WEAPON.AMMO_OPTIONAL;
		+WEAPON.NOALERT;
	}
	
	States 
	{
		Select:
			TNT1 A 0 
			{
				A_Overlay(-3, "WeaponIn");
				A_Overlay(3, "SubWeaponWaiting");
				invoker.PsyCharge = 0;
			}
			TNT1 A 1 A_Raise(99);
			Loop;
			
		Deselect:
			TNT1 A 0 A_Overlay(-3, "WeaponOut"); 
			TNT1 A 1 A_Lower(99);
			Loop;
			
		Ready:
			TNT1 A 1 
			{
				A_WeaponReady(WRF_ALLOWRELOAD | WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER2);
				A_OverlayFlags(-3, PSPF_ADDBOB, FALSE);
			}
			Loop;
			
		WeaponReady:
			Goto Ready;
			
		Fire:
			Goto Ready;
			
		SubWeaponWaiting:
			TNT1 A 0 
			{ 
				invoker.SubAmt = 0; 
				invoker.SubFiring = FALSE; 
				invoker.SataeSafe = FALSE;
			}
			TNT1 A 1 A_ListenAttackKeys(3);
			Loop;
	
		Reload: 
			TNT1 A 1
			{
				let plr = TheArachnotron(invoker.owner);
			
				plr.PowerReady ++;
				
				if (plr.PowerReady > isPossess)
				{
					plr.PowerReady = isForce;
				}
			}
			TNT1 A 9 A_Print(invoker.PsyReady.." readied");
			Goto Ready;
			
		AltFire:
			TNT1 A 1 
			{
				let plr = TheArachnotron(invoker.Owner);
				
				plr.isFocusing = TRUE;
				
				if(plr.CooldownTime) invoker.PsyCharge += 0.75;
				else invoker.PsyCharge ++;
				
				invoker.PsyCharge += (plr.PsiLevel * 0.25);
			}
			TNT1 A 0 A_ReFire();
			TNT1 A 0 A_JumpIf(invoker.PsyCharge >= invoker.FocusTarget, "PissPower");
			TNT1 A 0 
			{ 
				let plr = TheArachnotron(invoker.Owner);
				
				invoker.PsyCharge = 0; 	
				plr.isFocusing = FALSE;
			}
			Goto Ready;
			
		PissPower:
			TNT1 A 1
			{
				let plr = TheArachnotron(invoker.Owner);
				
				plr.PsyCharge = invoker.PsyCharge;
				
				switch(plr.PowerReady)
				{
					case isForce: A_PsiForce();
						break;
					case isRenew: A_PsiRenew();
						break;
					case isPossess: A_PsiBrainwash();
						break;
				}
				
				A_PsiOtherStuff();
				plr.isFocusing = FALSE;
				invoker.BrainCheck = FALSE;
			}
			Goto Ready;
			
		Zoom:
			TNT1 A 1
			{
				let plr = TheArachnotron(invoker.owner);
			
				plr.SubReady ++;
				
				if (plr.SubReady == 3)
				{
					plr.SubReady = 0;
				}
				if (plr.SubReady == 1 && !CheckInventory("HasSatae", 1))
				{
					plr.SubReady ++;
				}
				if (plr.SubReady == 2 && !CheckInventory("HasScorpion", 1))
				{
					plr.SubReady = 0;
				}
			}
			TNT1 A 9 A_Print(invoker.SubReady.." readied");
			Goto Ready;
			
		SubWeaponFire:
			TNT1 A 0 A_JumpIfInventory("HODSubAmmo", invoker.SubCost, "Arming");
			Goto SubWeaponOOA;
		Arming:
			TNT1 A 1
			{
				let plr = TheArachnotron(invoker.Owner);
				invoker.Subcharge ++;
				plr.isSubArming = TRUE;
			}
			TNT1 A 0 A_JumpIf(player.cmd.buttons & BT_USER1, "Arming");
			TNT1 A 0 A_JumpIf(invoker.SubCharge >= invoker.SubArm, "FireSubbyWubby");
			TNT1 A 0 { invoker.SubCharge = 0; }
			Goto SubWeaponWaiting;
		
		FireSubbyWubby:
			TNT1 A 0
			{
				let plr = TheArachnotron(invoker.Owner);
				
				if(plr.SubReady == isRockets)
				{
					FLineTraceData dat;
					bool checker = plr.LineTrace(0, 128, -90, TRF_THRUACTORS | TRF_NOSKY, player.viewz - player.mo.pos.z,  data: dat);
					
					if(Checker)
					{
						if(checker && dat.HitType == TRACE_HITCEILING)
						{
							invoker.SataeSafe = TRUE;
							invoker.SubCharge = 0;
							plr.isSubArming = FALSE;
							A_Print("WARNING\n\nLaunch clearance too low\nAborting firing");
							A_SetTics(35);
						}
					}
				}
			}
			TNT1 A 0 A_JumpIf(Invoker.SataeSafe, "SubWeaponWaiting");
			TNT1 A 0 A_TakeInventory("HODSubAmmo", invoker.SubCost);
			TNT1 A 0 { invoker.SubAmt = invoker.SubTarget; }
		FireLoop:
			TNT1 A 0 A_JumpIf(invoker.SubAmt <= 0, "SubWeaponWaiting");
			TNT1 A 0 A_JumpIf(TheArachnotron(invoker.Owner).SubReady == isLaser, "ChargeBEEMU");
			TNT1 A 1
			{
				let plr = TheArachnotron(invoker.Owner);
				plr.isSubArming = FALSE;
				invoker.SubFiring = TRUE;
			
				switch(plr.SubReady)
				{
					case isGrenades: A_SubGrenande();
						break;
					case isRockets: A_SubRockets();
						break;
					case isLaser: A_SubLaser();
						break;
				}
				A_SubOtherStuff();
			}
			Loop;
			
		ChargeBEEMU:
			TNT1 A 0 A_StartSound("SCORCHAR", 88); 
		
		BEEEEEEM:
			TNT1 A 0 A_JumpIf(invoker.ScorpCharge > invoker.ScorpTarget, "Bamp");
			TNT1 A 1
			{
				let plr = TheArachnotron(invoker.Owner);
				plr.isSubArming = FALSE;
			
				invoker.ScorpUp = TRUE;
				invoker.ScorpCharge ++;
				
				A_SetSpeed(0.25);
			}
			Loop;
			
		Bamp:
			TNT1 A 0 A_JumpIf(invoker.SubAmt <= 0, "COOLDAHLASER");
			TNT1 A 1
			{
				invoker.ScorpUp = FALSE;
				invoker.SubFiring = TRUE;
				A_SetSpeed(0);
				A_SubLaser();
				A_SubOtherStuff();
			}
			Loop;
			
		COOLDAHLASER:
			TNT1 A 15 A_SetSpeed(0.25);
			TNT1 A 35
			{
				A_StartSound("SCORCOOL", 88);
				invoker.ScorpCharge = 0;
			}
			TNT1 A 0 A_SetSpeed(1);
			Goto SubWeaponWaiting;
		
		MainOOA:
			#### # 7 A_Print("Warning\n\nInsufficent Primary Ammo");
			TNT1 A 0 A_Jump(256, "WeaponReady");
			Goto WeaponReady;
		
		SubWeaponOOA:
			TNT1 A 7 A_Print("Warning\n\nInsufficent Auxiliary Ammo");
			Goto SubWeaponWaiting;
	}
}
	
class MindForceShot : FastProjectile 
{
	int PushTarget;
	int ExecuteTarget;
	
	override void PostBeginPlay()
	{
		if(TheArachnotron(target).PsyCharge > 245)
		{	
			PushTarget 		= 999999;
			ExecuteTarget	= 1000;
		}				
		else if(TheArachnotron(target).PsyCharge > 105)
		{	
			PushTarget 		= 1000;
			ExecuteTarget	= 400;
		}
		else if(TheArachnotron(target).PsyCharge > 60)
		{	
			PushTarget 		= 700;
			ExecuteTarget	= 150;
		}
		else if(TheArachnotron(target).PsyCharge > 20)
		{	
			PushTarget 		= 500;
			ExecuteTarget	= 70;
		}
		else if(TheArachnotron(target).PsyCharge < 20)
		{	
			PushTarget 		= 300;
			ExecuteTarget	= 50;
		}
	
		super.PostBeginPlay();
	}

	override int DoSpecialDamage(actor target, int damage, name damagetype)
	{
		if(target && target.bISMONSTER)
		{
			if(target.SpawnHealth() >= self.ExecuteTarget && target.SpawnHealth() < self.PushTarget)
			{
				Damage = 0;
				self.bEXTREMEDEATH = FALSE;
				target.GiveInventory("ForceSpeedGiver", 1);
				target.GiveInventory("ForceDefGiver", 1);
				if(target.bDONTTHRUST == FALSE)
				{
					target.A_ChangeVelocity(-15, 0, 10, CVF_RELATIVE | CVF_REPLACE);
				}
			}
			
			if(target.SpawnHealth() >= self.PushTarget)
			{
				Damage = 0;
				self.bCAUSEPAIN = FALSE;
				self.bFORCEPAIN = FALSE;
				self.target.A_Print("Force failed \nTarget too powerful");
			}
		}
	
		return super.DoSpecialDamage(target, damage, damagetype);
	}

	Default 
	{
		Speed 250;
		Radius 8;
		Height 16;
		DamageFunction ExecuteTarget;
		ProjectileKickBack 1000;
		
		DamageType "Melee";
	
		+PIERCEARMOR;
		+INVISIBLE;
		+EXTREMEDEATH;
		+CAUSEPAIN;
		+FORCEPAIN;
		+BLOODLESSIMPACT;
		+MTHRUSPECIES;
	}
	
	States 
	{
		Spawn:
			TNT1 A 1 NoDelay A_SpawnItemEx("ForceWave");
			Loop;
			
		Death:
		Crash:
			TNT1 A 0 
			{
				A_SpawnItemEx("HODA_BigDebrisSmoke");
			
				if((pos.z < floorz + 4 && !GetFloorTerrain().IsLiquid))
				{
					for (int i = 0; i < random(1, 3); ++i)
						A_SpawnItemEx("HODA_BigDebrisSmoke", frandom(-16, 16), frandom(-16, 16), frandom(3, 16), 0, random(-4, 4), random(4, 16));
				}
			
				if(pos.z > floorz + 4 && pos.z < ceilingz - 10)
				{
					for (int i = 0; i < random(1, 3); ++i)
						A_SpawnItemEx("HODA_BigDebrisSmoke", random(-16, 0), random(-16, 16), random(-16, 16), random(0, -4), random(-4, 4), random(-24, 24));
				}
				
				if(pos.z > ceilingz - 10)
				{
					for (int i = 0; i < random(1, 3); ++i)
						A_SpawnItemEx("HODA_BigDebrisSmoke", frandom(-16, 16), frandom(-16, 16), frandom(-16, -3), random(-4, 4), random(-4, 4), random(-12, 0));
				}

			}
		XDeath:
			TNT1 A 0
			{
				for (int i = 0; i < 200; ++i)
				{
					if(self.ExecuteTarget >= 1000) 
					{
						FLineTraceData Pushee;
						bool checker = self.LineTrace(random(0, 360), 256, random(-35, 35), data: Pushee);
						
						if(checker && Pushee.HitActor)
						{
							if(Pushee.HitActor.bISMONSTER)
							{
								if(Pushee.HitActor.SpawnHealth() < 300)
								Pushee.HitActor.A_Die("EXTREME");
							}
						}
					}
					else if(self.ExecuteTarget >= 400) 
					{
						FLineTraceData Pushee;
						bool checker = self.LineTrace(random(0, 360), 64, random(-35, 35), data: Pushee);
						
						if(checker && Pushee.HitActor)
						{
							if(Pushee.HitActor.bISMONSTER)
							{
								if(Pushee.HitActor.SpawnHealth() < 70)
								Pushee.HitActor.A_Die("EXTREME");
							}
						}
					}
				}
			}
			TNT1 A 1 A_SpawnItemEx("BigForceWave");
			Stop;
	}
}

class ForceWave : Actor 
{
	override void BeginPlay()
	{
		A_SetRoll(0, 360);
	
		super.BeginPlay();
	}

	Default 
	{
		Alpha 0.15;
		
		Renderstyle "Add";
	
		+NOINTERACTION;
		+FORCEXYBILLBOARD;
		+ROLLSPRITE;
		+ROLLCENTER;
	}
	
	States 
	{
		Spawn:
			SSHK Z 1 NoDelay
			{
				A_SetRoll(Roll + 40);
				A_FadeTo(0.0, 0.015, TRUE);
				A_SetScale(Scale.X + 0.1, Scale.Y + 0.1);
			}
			Loop;
	}
}

class BigForceWave : ForceWave
{
	Default 
	{
		Scale 2.25;
		Alpha 0.20;
		
		-ROLLSPRITE;
	}
}

class ForceSpeedGiver : PowerupGiver
{
	Default 
	{
		Powerup.Type "ForceSpeedReduction";
		Powerup.Duration -10;
		
		+Inventory.AUTOACTiVATE;
		+Inventory.AlwaysPickUp;
	}
	
	States 
	{
		Pickup:
			TNT1 A 3 A_SetSpeed(Speed/4);
			Stop;
	}
}

class ForceDefGiver : PowerupGiver 
{
	Default 
	{
		Powerup.Type "ForceDefReduction";
		Powerup.Duration -10;
		
		+INVENTORY.AUTOACTiVATE;
		+Inventory.ALWAYSPICKUP;
	}
}

class ForceSpeedReduction : PowerSpeed 
{
	Default
	{
		Powerup.Duration -10;
		Speed 0.25;
	}
}

class ForceDefReduction : PowerProtection 
{
	Default 
	{
		DamageFactor "Normal", 2.0;
	}
}