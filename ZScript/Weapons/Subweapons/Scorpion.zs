class ScorpPuff : Actor 
{
	int BurnTimer;
	
	override void PostBeginPlay()
	{
		A_SetRoll(frandom(0, 360));
		bSPRITEFLIP = random(0, 1);
	
		super.PostBeginPlay();
	}

	Default 
	{
		Height 6;
	
		Scale 0.5;
		
		Renderstyle "Add";
		
		+BRIGHT;
		+ROLLSPRITE;
		+NOINTERACTION;
		+ALWAYSPUFF;
		+ALLOWTHRUFLAGS;
		+NODAMAGETHRUST;
		+MTHRUSPECIES;
	}
	
	States 
	{
		Spawn:
			TNT1 A 0;
			
		Death:
		XDeath:
			TNT1 A 0;
			Goto BioBlast;
		Crash:
			TNT1 A 0 A_JumpIf(self.BurnTimer >= 45, "Explodet");
			SFVX W 1
			{
				self.BurnTimer ++;
				
				A_SetScale(Scale.X + (0.05 * (self.BurnTimer/double(35))));
				
				if(pos.z > floorz + 4 && pos.z < ceilingz - 8)
				{	
					bWALLSPRITE = TRUE;
					
					if(self.BurnTimer > 10) A_SpawnItemEx("HODA_Fire", frandom(-4, 0), frandom(-4, 4), frandom(-4, 4));
					if(self.BurnTimer > 20) A_SpawnItemEx("HODA_Fire", frandom(-4, 0), frandom(-4, 4), frandom(-4, 4));
					if(self.BurnTimer > 30) A_SpawnItemEx("HODA_Fire", frandom(-4, 0), frandom(-4, 4), frandom(-4, 4));
					
				}
				else 
				{
					bFLATSPRITE = TRUE;
					
					if(pos.z < floorz + 4)
					{
						if(self.BurnTimer > 10) A_SpawnItemEx("HODA_Fire", zofs: frandom(0, 12));
						if(self.BurnTimer > 20) A_SpawnItemEx("HODA_Fire", zofs: frandom(0, 12), zvel: random(0, 3));
						if(self.BurnTimer > 30) A_SpawnItemEx("HODA_Fire", zofs: frandom(0, 12), zvel: random(0, 5));
					}
					
					if(pos.z > ceilingz - 8)
					{
						if(self.BurnTimer > 10) A_SpawnItemEx("HODA_Fire", zofs: frandom(-12, 0));
						if(self.BurnTimer > 20) A_SpawnItemEx("HODA_Fire", zofs: frandom(-12, 0), zvel: random(-3, 0));
						if(self.BurnTimer > 30) A_SpawnItemEx("HODA_Fire", zofs: frandom(-12, 0), zvel: random(-5, 0));
					}
				}
			}
			Loop;
			
		Explodet:
			TNT1 A random(0, 3)
			{
				A_SpawnItemEx("HEExplosion");
				
				for (int i = 0; i < 3; ++i)
					A_SpawnItemEx("BurningMetal", 0, 0, 8, frandom(-30, 30), frandom(-30, 30), frandom(-30, 30));
					
				A_Explode(196, 128, XF_HURTSOURCE | XF_EXPLICITDAMAGETYPE, damagetype: "Fire");
				A_StartSound("SCORBOOM", random(89, 99));
			}
			TNT1 A random(0, 3) { if(self.BurnTimer) A_SpawnItemEx("HEExplosion", frandom(-16, 16), frandom(-16, 16), frandom(-16, 16)); }
			TNT1 A random(0, 3) { if(self.BurnTimer) A_SpawnItemEx("HEExplosion", frandom(-32, 32), frandom(-32, 32), frandom(-32, 32)); }
			Stop;
			
		BioBlast:
			TNT1 A 0
			{
				bEXTREMEDEATH = TRUE;
				bFORCERADIUSDMG = TRUE;
			
				A_Explode(64, 32, XF_HURTSOURCE | XF_EXPLICITDAMAGETYPE, damagetype: "Fire");
				A_StartSound("SCORBOOM", random(89, 99));
				Roll = frandom(0, 360);
				A_SetRenderstyle(1.0, STYLE_ADD);
				A_SetScale(0.8);
					
				if(HODA_Smoke) { A_SpawnItemEx("HODA_ExplosionSmoke", frandom(-16, 16), frandom(-16, 16), frandom(0, 16), 0, 0, random(0, 2)); }
				if(HODA_Embers)
				{
					for (int i = 0; i < random(1, 4); ++i)
						A_SpawnProjectile("HODA_Spark", 0, 0, frandom(0, 360), CMF_AIMDIRECTION, frandom(0, 360));
				}
			}
			MVEX ABCDEFGHIJKLM 1 Bright;
			Stop;
	}
}

class ScorpLaserBall : Actor 
{
	Default 
	{
		Renderstyle "Add";
		Scale 0.1;
	
		+NOINTERACTION;
		+BRIGHT;
	}
	
	States 
	{
		Spawn:
			SPKB E 2 NoDelay;
			Stop;
	}
}

class ScorpSpawn : CustomInventory replaces PlasmaRifle 
{
	Default 
	{
		Inventory.PickUpMessage "Scorpion Tail Cannon installed";
		Inventory.PickUpSound "";
		Scale 0.1;
	}
	
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasScorpion", 1, "SpawnAmmo", AAPTR_PLAYER1);
			SCOR A 1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODSubAmmo");
			Stop;
			
		PickUp:
			TNT1 A 0
			{
				A_GiveInventory("HasScorpion", 1);
				A_GiveInventory("HODSubAmmo", 25);
				A_StartSound("SubUp", CHAN_AUTO);
			}
			Stop;
	}
}

class HasScorpion : Inventory
{
	Default 
	{
		Inventory.MaxAmount 1;
		Inventory.Amount 1;
	}
}