class Prosoma : Actor 
{
	Default 
	{
		Projectile;
		
		Damage 0;
		Speed 35;
		Height 8;
		Radius 4;
		
		Scale 0.5;
		
		DamageType "Fire";
		
		BounceType "Doom";
		BounceCount 4;
		BounceFactor 0.35;
		WallBounceFactor 0.25;
		
		+ROLLSPRITE;
		+BOUNCEONFLOORS;
		+BOUNCEONWALLS;
		+BOUNCEONCEILINGS;
		+MTHRUSPECIES;
		-NOGRAVITY;
	}
	
	States 
	{
		Spawn:
			SOMA AAABBB 2 A_SetRoll(Roll - 35);
			Loop;
			
		Death:
			SOMA ABA 1 
			{
				if(pos.z > floorz + 4) A_SetTics(0);
				
				A_SetRoll(Roll - 55);
				A_ChangeVelocity(z: 12);
			}
			TNT1 A 0
			{
				A_Explode(128, 128, XF_HURTSOURCE | XF_EXPLICITDAMAGETYPE, TRUE, damagetype: "Fire"); 
			
				bNOGRAVITY = TRUE;
				Roll = frandom(0, 360);
				A_SetRenderstyle(1.0, STYLE_ADD);
				A_SetScale(1.0);
				A_StartSound("Prosoma/Explode", random(69, 79), CHANF_DEFAULT, 1.0, 0.5);
				A_ScaleVelocity(0);
				
				if(HODA_Smoke) { A_SpawnItemEx("HODA_ExplosionSmoke", frandom(-16, 16), frandom(-16, 16), frandom(0, 16), 0, 0, random(0, 2)); }
				if(HODA_Embers)
				{
					for (int i = 0; i < random(1, 4); ++i)
						A_SpawnProjectile("HODA_Spark", 0, 0, frandom(0, 360), CMF_AIMDIRECTION, frandom(0, 360));
				}
			}
			MVEX ABCDEFGHIJKLM 1 Bright;
			Stop;
	}
}