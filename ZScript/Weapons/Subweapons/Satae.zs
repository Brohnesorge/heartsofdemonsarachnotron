class Satae : Actor
{
	Default 
	{
		Projectile;
	
		DamageFunction 35;
		Speed 1;
		Height 8;
		Radius 4;
		
		Scale 0.20;
		
		DamageType "Piercing";
		Decal "SataeHole";
		
		+ROLLSPRITE;
		+ROLLCENTER;
		+SEEKERMISSILE;
		+SCREENSEEKER;
		+MTHRUSPECIES;
	}
	
	States 
	{
		Spawn:
			XMIS A 35 NoDelay;
			XMIS A 1 
			{
				A_SeekerMissile(65, 120, SMF_LOOK| SMF_PRECISE, 256, 50);
				A_FaceTracer();
				A_ChangeVelocity(25, flags: CVF_RELATIVE);
				A_SetSpeed(25);
				A_StartSound("weapons/Satae", random(69, 79));	
			}
			XMIS A 0
			{
				if(self.tracer)
				{
					self.tracer.A_SpawnItemEx("SataeLock", flags: SXF_SETMASTER); 
				}
			}
			
		Ligma:
			XMIS BCB 1 Bright
			{
				A_SeekerMissile(25, 35, SMF_LOOK | SMF_PRECISE, 16, 20);
				A_SpawnItemEx("HODA_SataeSmoke", -8);
			}
			XMIS C 1 Bright
			{
				A_SeekerMissile(25, 35, SMF_LOOK | SMF_PRECISE, 16, 20);
				A_SpawnItemEx("HODA_SataeSmoke", -8);
				
				if(self.tracer)
				{
					self.tracer.A_SpawnItemEx("SataeLock", flags: SXF_SETMASTER); 
				}
			}
			Loop;
			
		Death:
			TNT1 A 0
			{
				A_Explode(128, 128, XF_HURTSOURCE | XF_EXPLICITDAMAGETYPE, TRUE, damagetype: "Fire"); 
				A_SetRenderstyle(1.0, STYLE_ADD);
				A_SetScale(1.0);
				Roll = frandom(0, 360);
				A_StartSound("Satae/Explode", random(69, 79), CHANF_DEFAULT, 1.0, 0.65);
				A_StartSound("Satae/Fxplode", random(69, 79), CHANF_DEFAULT, 1.0, ATTN_NONE);
				
				if(HODA_Smoke) { A_SpawnItemEx("HODA_ExplosionSmoke", frandom(-16, 16), frandom(-16, 16), frandom(0, 16), 0, 0, random(0, 2)); }
				if(HODA_Embers)
				{
					for (int i = 0; i < random(1, 4); ++i)
						A_SpawnProjectile("HODA_Spark", 0, 0, frandom(0, 360), CMF_AIMDIRECTION, frandom(0, 360));
				}
			}
			MVEX ABCDEFGHIJKLM 1 Bright;
			Stop;
	}
}

class SataeLock : Actor 
{
	override void tick()
	{
		Alpha = frandom(0.35, 0.85);
		
		if(master)
		{
			A_Warp(AAPTR_MASTER, zofs: master.height * 0.75);
		}
		
		if(!master || master.health < 1) { self.Destroy(); }
	
		super.tick();
	}

	Default 
	{
		Scale 1.00;
		
		Renderstyle "Translucent";
		
		+BRIGHT;
		+NOINTERACTION;
	}
	
	States 
	{
		Spawn:
			XLOK A 4 NoDelay;
			Stop;
	}
}

class SataeSpawn : CustomInventory
{
	Default 
	{
		Inventory.PickUpMessage "Satae Micro Missile Launcher installed";
		Inventory.PickUpSound "";
		Scale 0.75;
	}
	
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasSatae", 1, "SpawnAmmo", AAPTR_PLAYER1);
			MMMB A 30;
			MMMB BCDE 1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODSubAmmo");
			Stop;
			
		PickUp:
			TNT1 A 0
			{
				A_GiveInventory("HasSatae", 1);
				A_GiveInventory("HODSubAmmo", 10);
				A_StartSound("SubUp", CHAN_AUTO);
			}
			Stop;
	}
}
 
class HasSatae : Inventory 
{
	Default 
	{
		Inventory.MaxAmount 1;
		Inventory.Amount 1;
	}
}