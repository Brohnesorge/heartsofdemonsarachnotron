class HOD_Goliath : ArachnoBaseWeapon
{
	Default
	{
		Tag "Goliath Chaingun";
		ArachnoBaseWeapon.ShortTag "Goliath";
		Weapon.SlotNumber 	6;
		Weapon.AmmoType 	"HODMainAmmo";
		Weapon.AmmoUse		3;
	}
	
	States
	{
		WeaponReady:
			HCGG A 1 A_JumpIfInventory("HasDoubleGoliath", 1, "DoubleWeaponReady");
			REPG A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponFire");
			Loop;
			
		DoubleWeaponReady:
			2CGG A 1 A_Overlay(4, "OtherWeapon");
			REPG A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponDoubleFire");
			Loop;
			
		OtherWeapon:
			3CGG A 1;
			Loop;
			
		WeaponIn:
			TNT1 A 0 
			{
				A_OverlayOffset(-3, 0, 150);
				A_OverlayOffset(4, 0, 150);
			}
			HCGG AAAAA 1 
			{
				A_OverlayOffset(-3, 0, -30, WOF_ADD);
				A_OverlayOffset(4, 0, -30, WOF_ADD);
			}
			Goto WeaponReady;
			
		WeaponOut:
			HCGG AAA 1 
			{
				A_OverlayOffset(-3, 0, 30, WOF_ADD);
				A_OverlayOffset(4, 0, 30, WOF_ADD);
			}
			Stop;
			
		WeaponFire:
			HCGG A 0 A_JumpIfNoAmmo("MainOOA");
			HCGG A 0 A_StartSound("WindUp", CHAN_WEAPON);
			HCGG ABCD 3;
			HCGG ABCD 2;
			HCGG ABCD 1;
			
		FireLoop:
			TNT1 A 0 A_JumpIfNoAmmo("SpinDown");
			HCGF A 1 Bright 
			{
				A_FireBullets(0.5, 0.5, -1, random(20, 100), "GoliathExplosivePuff", FBF_NORANDOM | FBF_USEAMMO);
				A_AlertMonsters();
				A_StartSound("weapons/Goliath", CHAN_WEAPON);
				A_StartSound("GAULOOP", 45, CHANF_LOOPING);
			}
			HCGG B 1;
			HCGF F 1 Bright
			{
				A_FireBullets(3.5, 3.5, -1, random(20, 100), "GoliathPuff", FBF_NORANDOM);
				A_AlertMonsters();
				A_StartSound("weapons/Goliath", CHAN_WEAPON);
			}
			HCGG D 1 A_FireBullets(3.5, 3.5, -1, random(20, 100), "GoliathPuff", FBF_NORANDOM); 
			TNT1 A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "FireLoop");
			Goto SpinDown;
		
		SpinDown:
			HCGG ABCD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "FireLoop");
			TNT1 A 0 A_StartSound("GAUDOWN", 45);
			HCGG AABBCCDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "FireLoop");
			HCGG AAABBBCCCDDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "FireLoop");
			HCGG AAABBBCCCDDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "FireLoop");
			HCGG AAABBBCCCDDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "FireLoop");
			HCGG AAAABBBBCCCCDDDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "FireLoop");
			Goto WeaponReady;
			
		WeaponDoubleFire:
			2CGG A 0 A_JumpIfNoAmmo("MainOOA");
			TNT1 A 0 A_Overlay(4, "OtherFire");
			TNT1 A 0 A_StartSound("WindUp", CHAN_WEAPON);
			2CGG ABCD 3;
			2CGG ABCD 2;
			2CGG ABCD 1;
			
		DoubleFireLoop:
			2CGG A 0 A_JumpIfNoAmmo("DoubleSpinDown");
			TNT1 A 0 A_Overlay(4, "OtherFireLoop");
			2CGF A 1 Bright 
			{
				A_FireBullets(0.5, 0.5, -1, random(20, 100) * 0.75, "GoliathExplosivePuff", FBF_NORANDOM | FBF_USEAMMO);
				A_AlertMonsters();
				A_StartSound("weapons/Goliath", CHAN_WEAPON);
				A_StartSound("GAULOOP", 45, CHANF_LOOPING);
				A_StartSound("GAULOOP", 46, CHANF_LOOPING);
			}
			2CGG B 1;
			2CGF B 1 Bright
			{
				A_FireBullets(3.5, 3.5, -1, random(20, 100) * 0.75, "GoliathPuff", FBF_NORANDOM);
				A_AlertMonsters();
				A_StartSound("weapons/Goliath", CHAN_WEAPON);
			}
			2CGG D 1 A_FireBullets(3.5, 3.5, -1, random(20, 100) * 0.75, "GoliathPuff", FBF_NORANDOM);
			TNT1 A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "DoubleFireLoop");
			Goto DoubleSpinDown;
			
		DoubleSpinDown:
			2CGG ABCD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "DoubleFireLoop");
			TNT1 A 0 A_StartSound("GAUDOWN", 45);
			TNT1 A 0 A_StartSound("GAUDOWN", 46);
			2CGG AABBCCDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "DoubleFireLoop");
			2CGG AAABBBCCCDDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "DoubleFireLoop");
			2CGG AAABBBCCCDDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "DoubleFireLoop");
			2CGG AAABBBCCCDDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "DoubleFireLoop");
			2CGG AAAABBBBCCCCDDDD 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK && CheckInventory("HODMainAmmo", 3), "DoubleFireLoop");
			Goto DoubleWeaponReady;
			
		OtherFire:
			3CGG ABCD 3;
			3CGG ABCD 2;
			3CGG ABCD 1;
		OtherFireLoop:
			3CGF A 1 Bright 
			{
				A_FireBullets(0.5, 0.5, -1, random(20, 100) * 0.75, "GoliathExplosivePuff", FBF_NORANDOM);
				A_AlertMonsters();
				A_StartSound("weapons/Goliath", CHAN_WEAPON);
			}
			3CGG B 1;
			3CGF B 1 Bright
			{
				A_FireBullets(3.5, 3.5, -1, random(20, 100) * 0.75, "GoliathPuff", FBF_NORANDOM);
				A_AlertMonsters();
				A_StartSound("weapons/Goliath", CHAN_WEAPON);
			}
			3CGG D 1 A_FireBullets(3.5, 3.5, -1, random(20, 100) * 0.75, "GoliathPuff", FBF_NORANDOM); 
			3CGG ABCD 1;
			3CGG ABCD 2;
			3CGG ABCDABCDABCD 3;
			3CGG ABCD 4;
			Goto OtherWeapon;
	}
}

class GoliathExplosivePuff : Actor 
{
	Default 
	{
		+NOINTERACTION;
		+PUFFONACTORS;
		+ALWAYSPUFF;
		+EXTREMEDEATH;
		+MTHRUSPECIES;
		
		Scale 0.5;
		Alpha 1.0;
		
		Renderstyle "Add";
		DamageType "Piercing";
		Decal "GolHole";
	}
	
	States
	{
		Spawn:
		Death:
		Crash:
		XDeath:
			TNT1 A 0 NoDelay
			{
				A_Explode(64, 64, XF_HURTSOURCE | XF_EXPLICITDAMAGETYPE, TRUE, 0, 0, 0, "BulletPuff", "Fire"); 
				bSpriteFlip = random(0, 1);
				A_StartSound("Goliath/Explode", random(69, 79), CHANF_DEFAULT, 1.0, 0.5);
				A_SetScale(frandom(0.35, 0.6));
			}
			MVEX ABCDEFGHIJKLM 1 Bright;
			Stop;
	}
}

class GoliathPuff : Actor 
{
	Default 
	{
		+NOINTERACTION;
		+MTHRUSPECIES;
		
		Scale 0.25;
		Alpha 1.0;
		
		Renderstyle "Add";
		DamageType "LongArms";
		Decal "ArachnoBulletHole";
	}
	
	States 
	{
		Spawn:
		Death:
		Crash:
			TNT1 A 0
			{
				A_SetRenderstyle(1.0, STYLE_ADD);
				A_SetScale(0.1);
			}
			SPKO CCC 1 Bright A_SetScale(Scale.X + 0.15);
			Stop;
		XDeath:
			TNT1 A 0;
			Stop;
	}
}

class HasDoubleGoliath : Inventory
{
	Default 
	{
		inventory.Amount 1;
		Inventory.MaxAmount 1;
	}
}

class GoliathSpawn : CustomInventory replaces BFG9000
{
	Default
	{
		inventory.PickupMessage "Goliath Super Chaingun installed";
		Inventory.PickUpSound "";
	}

	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HOD_Goliath", 1, "SpawnTwinlinked", AAPTR_PLAYER1);
			HCNG A 1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODMainAmmo");
			Stop;
			
		SpawnTwinlinked:
			TNT1 A 0 A_JumpIf(bDropped == TRUE, "SpawnAmmo");
			TNT1 A 0 A_SpawnItemEx("OtherGoliathSpawn");
			Stop;
			
		Pickup:
			TNT1 A 0 
			{
				A_GiveInventory("HOD_Goliath", 1);
				A_GiveInventory("HODMainAmmo", 30);
				A_StartSound("WEPUP", CHAN_AUTO);
			}
			Stop;
	}
}

class OtherGoliathSpawn : CustomInventory
{
	Default
	{
		inventory.PickupMessage "Twin-linked Goliath installed";
		inventory.PickUpSound "DOUBUP";
		+SPRITEFLIP;
	}

	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasDoubleGoliath", 1, "SpawnAmmo", AAPTR_PLAYER1);
			HCNG A 1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODMainAmmo");
			Stop;
			
		Pickup:
			TNT1 A 0 
			{
				A_GiveInventory("HasDoubleGoliath", 1);
				A_GiveInventory("HODMainAmmo", 30);
				A_StartSound("DOUBUP", CHAN_AUTO);
			}
			Stop;
	}
}