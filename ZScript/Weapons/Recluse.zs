class HOD_Recluse : ArachnoBaseWeapon
{
	int BeamCharge;
	int BeamThreshold;
	int ThresholdAmount;
	
	override void BeginPlay()
	{
		BeamCharge = 0;
		BeamThreshold = 275;
		ThresholdAmount = 4;
	
		super.BeginPlay();
	}
	
	action void A_FireRecluse()
	{
		if(invoker.BeamCharge < invoker.BeamThreshold) 
			A_FireProjectile("RecluseBall", 0, TRUE, 0, -3);
		else if(invoker.BeamCharge > Invoker.BeamThreshold + (invoker.ThresholdAmount * 4)) 
			A_RailAttack(50, 0, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.0, 0, "ReculseLaserBall5", -3);
		else if(invoker.BeamCharge > Invoker.BeamThreshold + (invoker.ThresholdAmount * 3)) 
			A_RailAttack(35, 0, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall3", -3);
		else if(invoker.BeamCharge > Invoker.BeamThreshold + (invoker.ThresholdAmount * 2)) 
			A_RailAttack(8, 0, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall2", -3);
		else if(invoker.BeamCharge > Invoker.BeamThreshold + (invoker.ThresholdAmount * 1)) 
			A_RailAttack(8, 0, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall1", -3);
		else if(invoker.BeamCharge > invoker.BeamThreshold) 
			A_RailAttack(8, 0, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall0", -3);
		
		A_StartSound("Weapons/Recluse",  CHAN_WEAPON);
		A_AlertMonsters();
		invoker.BeamCharge ++;
	}
	
	action void A_FireRecluseDouble()
	{
		int Offxet = 6;
	
		if(invoker.BeamCharge < invoker.BeamThreshold) 
		{
			A_FireProjectile("RecluseBall", 0, TRUE, Offxet, -3);
			A_FireProjectile("RecluseBall", 0, FALSE, -Offxet, -3);
		}
		
		else if(invoker.BeamCharge > Invoker.BeamThreshold + (invoker.ThresholdAmount * 4)) 
		{
			A_RailAttack(50, Offxet, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.0, 0, "ReculseLaserBall5", -3);
			A_RailAttack(50, -Offxet, FALSE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.0, 0, "ReculseLaserBall5", -3);
		}
		else if(invoker.BeamCharge > Invoker.BeamThreshold + (invoker.ThresholdAmount * 3))
		{
			A_RailAttack(35, Offxet, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall3", -3);
			A_RailAttack(35, -Offxet, FALSE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall3", -3);
		}
		else if(invoker.BeamCharge > Invoker.BeamThreshold + (invoker.ThresholdAmount * 2)) 
		{
			A_RailAttack(8, Offxet, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall2", -3);
			A_RailAttack(8, -Offxet, FALSE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall2", -3);
		}
		else if(invoker.BeamCharge > Invoker.BeamThreshold + (invoker.ThresholdAmount * 1)) 
		{
			A_RailAttack(8, Offxet, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall1", -3);
			A_RailAttack(8, -Offxet, FALSE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall1", -3);
		}
		else if(invoker.BeamCharge > invoker.BeamThreshold) 
		{
			A_RailAttack(8, Offxet, TRUE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall0", -3);
			A_RailAttack(8, -Offxet, FALSE, "", "", RGF_SILENT | RGF_NOPIERCING, 0, "RecluseBeamPuff", 0, 0, 0 , 0, 1.75, 1.5, "ReculseLaserBall0", -3);
		}
		
		A_StartSound("Weapons/Recluse",  CHAN_WEAPON);
		A_AlertMonsters();
		invoker.BeamCharge ++;
	}
	
	Default
	{
		Tag "Recluse Plasma Caster";
		ArachnoBaseWeapon.ShortTag "Recluse";
		Weapon.SlotNumber 	3;
		Weapon.AmmoType 	"HODMainAmmo";
		Weapon.AmmoUse		1;
	}
	
	States
	{
		WeaponReady:
			KSAW A 0 A_JumpIfInventory("HasDoubleRecluse", 1, "DoubleWeaponReady");
			KSAW AAJJKKLLMMNNNNMLKJA 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponFire");
			KSAW A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponFire");
			Loop;
			
		DoubleWeaponReady:
			2KWA A 1 A_Overlay(4, "OtherWeapon");
			2KWA A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponDoubleFire");
			Loop;
			
		OtherWeapon:
			3KWA A 1;
			Loop;
			
		WeaponIn:
			TNT1 A 0 
			{
				A_OverlayOffset(-3, 0, 150);
				A_OverlayOffset(4, 0, 150);
			}
			KSAW AAAAA 1 
			{
				A_OverlayOffset(-3, 0, -30, WOF_ADD);
				A_OverlayOffset(4, 0, -30, WOF_ADD);
			}
			Goto WeaponReady;
			
		WeaponOut:
			KSAW AAA 1 
			{
				A_OverlayOffset(-3, 0, 30, WOF_ADD);
				A_OverlayOffset(4, 0, 30, WOF_ADD);
			}
			Stop;
			
		WeaponFire:
			KSAW A 0 A_JumpIfNoAmmo("MainOOA");
			KSAW A 0 A_Jump(128, "Fire2");
			KSAW E 1 Bright A_FireRecluse();
			Goto LoopBack;
		Fire2:
			KSAW D 1 Bright A_FireRecluse();
			Goto LoopBack;
			
		LoopBack:
			KSAW H 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponFire");
			KSAW CBA 3 { invoker.BeamCharge = 0; }
			Goto WeaponReady;
				
		WeaponDoubleFire:
			2KWA A 0 A_JumpIfNoAmmo("MainOOA");
			2KWA A 0 A_Overlay(4, "OtherFire");
			2KWA A 0 A_Jump(128, "DoubleFire2");
			2KWA E 1 Bright A_FireRecluseDouble();
			Goto LoopDoubleBack;
			
		DoubleFire2:
			2KWA D 1 Bright A_FireRecluseDouble();
			Goto LoopDoubleBack;
			
		LoopDoubleBack:
			2KWA E 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponDoubleFire");
			2KWA C 3 { invoker.BeamCharge = 0; } 
			2KWA B 3;
			2KWA CBA 3;
			Goto DoubleWeaponReady;
			
		OtherFire:
			3KWA E 0 A_Jump(128, "OtherFire2");
			3KWA E 1 Bright;
			Goto OtherEnd;
			
		OtherFire2:
			3KWA D 1 Bright;
			Goto OtherEnd;
			
		OtherEnd:
			3KWA CBA 3;
			Goto OtherWeapon;
	}
}

class RecluseBall : FastProjectile
{
	int FlameRadius;
	
	override void BeginPlay()
	{
		FlameRadius = 2;
		
		super.BeginPlay();
	}
	
	Default 
	{
		Radius 4;
		Height 8;
		Speed 55;
		DamageFunction 8 * random(1, 3);
		
		DamageType "Plasma";
		Decal "WallBurn";
		
		+BLOODLESSIMPACT;
		+MTHRUSPECIES;
	}
	
	States
	{
		Spawn:
			TNT1 AAAA 2 NoDelay 
			{
				A_SpawnItemEx("RecluseFlame", FlameRadius * frandom(0, 1), FlameRadius * frandom(-1, 1), FlameRadius * frandom(-1, 1));
				A_Explode(8, 16, XF_EXPLICITDAMAGETYPE | XF_NOALLIES | XF_THRUSTLESS, FALSE, 16, damagetype: "Plasma");
			}
			Stop;
			
			
		Death:
			TNT1 AAAAAA random(2, 5) 
			{
				A_SpawnItemEx("HODA_Fire", FlameRadius * frandom(-6, 6), FlameRadius * frandom(-6, 6), FlameRadius * frandom(-6, 6));
				A_Explode(4, 16, XF_EXPLICITDAMAGETYPE | XF_NOALLIES | XF_THRUSTLESS, FALSE, 0, damagetype: "Fire");
			}
			Stop;
	}
}

class RecluseBallTwin : RecluseBall
{
	Default 
	{
		DamageFunction (8 * random(1, 3)) * 0.50;
	}
}

class RecluseFlame : Actor 
{
	override void BeginPlay()
	{
		bSPRITEFLIP = random(0, 1);
		A_SetScale(Scale.X * frandom(0.85, 1.15), Scale.Y * frandom(0.85, 1.15));
		if(HODA_Embers && !random(0, 3))
		{
			for (int i = 0; i < random(0, 2); ++i)
				A_SpawnItem("HODA_PlasmaSpark", 12 * frandom(-1, 1), 12 * frandom(-1, 1), 12 * frandom(-1, 1));
		}
	
		super.BeginPlay();
	}

	Default 
	{
		Alpha	0.65;
		XScale	0.50;
		YScale	0.25;
	
		RenderStyle "Add";
	
		+NOINTERACTION;
		+ROLLSPRITE;
		+ROLLCENTER;
		+BRIGHT;
		+MTHRUSPECIES;
	}
	
	States 
	{
		Spawn:
			PFXB ABCDEFGHIJKLMNO 1;
			Stop;
	}
}

class ReculseLaserBall0 : Actor
{
	Default 
	{
		Renderstyle "Add";
		Scale 0.25;
		Alpha 0.35;
	
		+NOINTERACTION;
		+ROLLSPRITE;
		+ROLLCENTER;
		+BRIGHT;
	}
	
	States 
	{
		Spawn:
			PFXB AB 2 NoDelay
			{
				bSPRITEFLIP = random(0, 1);
				A_SetRoll(-35, 35);
			}
			Stop;
	}
}

class ReculseLaserBall1 : ReculseLaserBall0 { Default { Scale 0.15; } }
class ReculseLaserBall2 : ReculseLaserBall0 { Default { Scale 0.10; } }
class ReculseLaserBall3 : ReculseLaserBall0 { Default { Scale 0.05; } }

class ReculseLaserBall4 : Actor
{
	Default 
	{
		Renderstyle "Translucent";
		Scale 0.07;
		Alpha 0.75;
	
		+NOINTERACTION;
		+BLOODLESSIMPACT;
		+BRIGHT;
	}
	
	States 
	{
		Spawn:
			PFXG A 2 NoDelay;
			Stop;
	}
}

class ReculseLaserBall5 : ReculseLaserBall4
{
	Default 
	{
		RenderStyle "Add";
		Scale 0.02;
	}
}

class RecluseBeamPuff : Actor
{
	Default 
	{
		DamageType "Plasma";
		Decal "PlasmaScorch";
	
		+NOINTERACTION;
		+FORCEDECAL;
		+ALWAYSPUFF;
		+BLOODLESSIMPACT;
		+ALLOWTHRUFLAGS;
		+MTHRUSPECIES;
	}
	
	States 
	{
		Spawn:
			TNT1 A 0;
			
		Death:
		XDeath:
		Crash:
			TNT1 AAA random(1, 3) A_SpawnItemEx("HODA_Fire", frandom(-2, 2), frandom(-2, 2), frandom(-2, 2));
			Stop;
	}
}

class HasDoubleRecluse : Inventory
{
	Default 
	{
		inventory.Amount 1;
		Inventory.MaxAmount 1;
	}
}

class RecluseSpawn : CustomInventory replaces Shotgun
{
	Default
	{
		inventory.PickupMessage "Recluse Plasma Caster installed";
		Inventory.PickUpSound "";
	}

	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HOD_Recluse", 1, "SpawnTwinlinked", AAPTR_PLAYER1);
			KSWP A 1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODMainAmmo");
			Stop;
			
		SpawnTwinlinked:
			TNT1 A 0 A_JumpIf(bDropped == TRUE, "SpawnAmmo");
			TNT1 A 0 A_SpawnItemEx("OtherRecluseSpawn");
			Stop;
			
		Pickup:
			TNT1 A 0 
			{
				A_GiveInventory("HOD_Recluse", 1);
				A_GiveInventory("HODMainAmmo", 30);
				A_StartSound("WEPUP", CHAN_AUTO);
			}
			Stop;
	}
}

class OtherRecluseSpawn : CustomInventory
{
	Default
	{
		inventory.PickupMessage "Twin-linked Recluse installed";
		inventory.PickUpSound "";
		+SPRITEFLIP;
	}

	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasDoubleRecluse", 1, "SpawnAmmo", AAPTR_PLAYER1);
			KSWP A 1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODMainAmmo");
			Stop;
			
		Pickup:
			TNT1 A 0 
			{
				A_GiveInventory("HasDoubleRecluse", 1);
				A_GiveInventory("HODMainAmmo", 30);
				A_StartSound("DOUBUP", CHAN_AUTO);
			}
			Stop;
	}
}

