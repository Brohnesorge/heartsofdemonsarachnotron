class HOD_BlackWidow : ArachnoBaseWeapon
{
	Bool ReadyB;

	action void A_FireWidow(double xof)
	{
		A_FireProjectile(CheckInventory("HasDoubleWidow", 1) ? "BlackWidowNailTwin" : "BlackWidowNail", 1.5 * frandom(-1, 1), TRUE, xof, -3, pitch: 1.5 * frandom(-1, 1));
		A_StartSound("NAILFIRE",  CHAN_WEAPON);
		A_AlertMonsters();
		invoker.ReadyB = invoker.ReadyB ? FALSE : TRUE;
	}

	Default
	{
		Tag "Black Widow Heavy Penetrator";
		ArachnoBaseWeapon.ShortTag "B. Widow";
		Weapon.SlotNumber 	4;
		Weapon.AmmoType 	"HODMainAmmo";
		Weapon.AmmoUse		1;
	}
	
	States
	{
		WeaponReady:
			MCGN A 1 A_JumpIfInventory("HasDoubleWidow", 1, "DoubleWeaponReady");
			MCGN A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponFire");
			Loop;
			
		DoubleWeaponReady:
			2LMG A 1 A_Overlay(4, "OtherWeapon");
			2LMG A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponDoubleFire");
			Loop;
			
		OtherWeapon:
			3LMG A 1;
			Loop;
			
		WeaponIn:
			TNT1 A 0 
			{
				A_OverlayOffset(-3, 0, 150);
				A_OverlayOffset(4, 0, 150);
			}
			MCGN AAAAA 1 
			{
				A_OverlayOffset(-3, 0, -30, WOF_ADD);
				A_OverlayOffset(4, 0, -30, WOF_ADD);
			}
			Goto WeaponReady;
			
		WeaponOut:
			MCGN AAA 1 
			{
				A_OverlayOffset(-3, 0, 30, WOF_ADD);
				A_OverlayOffset(4, 0, 30, WOF_ADD);
			}
			Stop;
			
		WeaponFire:
			MCGN A 0 A_JumpIfNoAmmo("MainOOA");
			MCGN D 1 Bright A_FireWidow(0);
			MCGN BC 1;
			Goto WeaponReady;
			
		WeaponDoubleFire:
			2LMG A 0 A_JumpIfNoAmmo("MainOOA");
			2LMG D 1 A_FireWidow(4);
			2LMG B 1 A_Overlay(4, "OtherFire");
			2LMG C 1;
			2LMG AA 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponDoubleFire");
			Goto DoubleWeaponReady;
			
		OtherFire:
			3LMG D 1 
			{
				A_FireProjectile("BlackWidowNailTwin", 1.5 * frandom(-1, 1), FALSE, -4, -3, pitch: 1.5 * frandom(-1, 1));
				A_Alertmonsters();
			}
			3LMG BC 1;
			Goto OtherWeapon;
	}
}

class BlackWidowNail : FastProjectile
{
	/*
	override void tick()
	{
		A_Log("Damage Type: "..self.DamageType..
		"\nSpeed: "..self.speed..
		"\nRipper?: "..self.bRIPPER
		);
	
		super.tick();
	}
	*/
	
	Default 
	{
		Height 6;
		Radius 3;
		DamageFunction 3 * random(1, 6);
		Speed 250;
		RipperLevel 2;
		
		Scale 0.10;
		Alpha 1.00;
		
		DamageType "Piercing";
		Decal "ArachnoBulletHole";
	
		+RIPPER;
		+PIERCEARMOR;
		+NOBOSSRIP;
		+BLOODSPLATTER;
		+NODAMAGETHRUST;
		+NOEXTREMEDEATH;
		+ROLLSPRITE;
		+ROLLCENTER;
		+MTHRUSPECIES;
	}
	
	States 
	{
		Spawn:
			NLPJ A 0 NoDelay 
			{
				bPAINLESS = random(0, 1);
			}
			NLPJ A 3;
			NLPJ A -1
			{
				bPAINLESS = FALSE;
				damagetype = "Melee";
				bRIPPER = FALSE;
				A_ScaleVelocity(0.5);
			}
			Stop;
			
		Death:
		Crash:
			TNT1 A 1
			{
				A_SetRenderstyle(1.0, STYLE_ADD);
				A_SetScale(0.05);
				A_SetRoll(random(0, 360));
				
				if(pos.z > floorz + 4 && pos.z < ceilingz - 8)
				{	
					if (HODA_Embers)
					{
						for (int i = 0; i < random(0, 2); ++i)
							A_SpawnProjectile("HODA_Spark", 0, 0, frandom(-5, 5), CMF_AIMDIRECTION, frandom(-5, 5));
					}
					if (HODA_Smoke) { A_SpawnItemEx("HODA_DebrisSmoke", xvel: 3); }
				}
				else 
				{
					bFLATSPRITE = TRUE;
					
					if (HODA_Embers)
					{
						if(pos.z < floorz + 4)
						{	
							if (HODA_Smoke) { A_SpawnItemEx("HODA_DebrisSmoke", zvel: frandom(2, 4)); }
						
							for (int i = 0; i < random(0, 2); ++i)
								A_SpawnProjectile("HODA_Spark", 0, 0, frandom(0, 360), CMF_AIMDIRECTION, frandom(-95, 85));
						}
						
						if(pos.z > ceilingz - 8)
						{
							if (HODA_Smoke) { A_SpawnItemEx("HODA_DebrisSmoke", zvel: frandom(-3, 0)); }
						
							for (int i = 0; i < random(0, 2); ++i)
								A_SpawnProjectile("HODA_Spark", 0, 0, frandom(0, 360), CMF_AIMDIRECTION, frandom(185, 355));
						}
					}
				}
			}
			BTPF B 1 Bright;
			SPRK A 1 Bright A_SetScale(Scale.X + 0.1);
			SPKO C 1 Bright A_SetScale(Scale.X + 0.1);
			Stop;
		XDeath:
			TNT1 A 0;
			Stop;
	}
}

class BlackWidowNailTwin : BlackWidowNail
{
	Default 
	{
		DamageFunction (3 * random(1, 6)) * 0.66;
	}
}

class HasDoubleWidow : Inventory
{
	Default 
	{
		inventory.Amount 1;
		Inventory.MaxAmount 1;
	}
}

class WidowSpawn : CustomInventory replaces Chaingun
{
	Default
	{
		inventory.PickupMessage "Black Widow Heavy Penetrator installed";
		Inventory.PickUpSound "";
	}

	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HOD_BlackWidow", 1, "SpawnTwinlinked", AAPTR_PLAYER1);
			MCGN W 1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODMainAmmo");
			Stop;
			
		SpawnTwinlinked:
			TNT1 A 0 A_JumpIf(bDropped == TRUE, "SpawnAmmo");
			TNT1 A 0 A_SpawnItemEx("OtherWidowSpawn");
			Stop;
			
		Pickup:
			TNT1 A 0 
			{
				A_GiveInventory("HOD_BlackWidow", 1);
				A_GiveInventory("HODMainAmmo", 30);
				A_StartSound("WEPUP", CHAN_AUTO);
			}
			Stop;
	}
}

class OtherWidowSpawn : CustomInventory
{
	Default
	{
		inventory.PickupMessage "Twin-linked Black Widow installed";
		inventory.PickUpSound "DOUBUP";
		+SPRITEFLIP;
	}

	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasDoubleWidow", 1, "SpawnAmmo", AAPTR_PLAYER1);
			MCGN W 1;
			Loop;
		
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODMainAmmo");
			Stop;		
	
			
		Pickup:
			TNT1 A 0 
			{
				A_GiveInventory("HasDoubleWidow", 1);
				A_GiveInventory("HODMainAmmo", 30);
				A_StartSound("DOUBUP", CHAN_AUTO);
			}
			Stop;
	}
}