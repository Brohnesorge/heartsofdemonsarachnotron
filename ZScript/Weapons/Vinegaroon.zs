class HOD_Vinegaroon : ArachnoBaseWeapon
{
	Default
	{
		Tag "Vinegaroon Slug Cannon";
		ArachnoBaseWeapon.ShortTag "Vinegaroon";
		Weapon.SlotNumber 	5;
		Weapon.AmmoType 	"HODMainAmmo";
		Weapon.AmmoUse		10;
	}
	
	States
	{
		WeaponReady:
			ROON A 1;
			REPG A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponFire");
			Loop;
			
		WeaponIn:
			TNT1 A 0 
			{
				A_OverlayOffset(-3, 0, 150);
				A_OverlayOffset(4, 0, 150);
			}
			ROON AAAAA 1 
			{
				A_OverlayOffset(-3, 0, -30, WOF_ADD);
				A_OverlayOffset(4, 0, -30, WOF_ADD);
			}
			Goto WeaponReady;
			
		WeaponOut:
			ROON AAA 1 
			{
				A_OverlayOffset(-3, 0, 30, WOF_ADD);
				A_OverlayOffset(4, 0, 30, WOF_ADD);
			}
			Stop;
			
		WeaponFire:
			ROON A 0 A_JumpIfNoAmmo("MainOOA");
			ROFN A 1
			{
				A_FireBullets(0, 0, 1, 100, "VinegaroonFirstPuff", FBF_NORANDOM | FBF_NORANDOMPUFFZ | FBF_USEAMMO);
				A_FireBullets(0, 0, 1, 500, "VinegaroonPuff", FBF_NORANDOM | FBF_NORANDOMPUFFZ | FBF_USEAMMO);
				A_StartSound("PROXFIRE",  CHAN_WEAPON);
				A_AlertMonsters();
			}
			ROFN ABCDEFGHIJKLMNO 1;
			ROFN O 6;
			ROON BA 3;
			Goto WeaponReady;
	}
}

class VinegaroonPuff : Actor 
{
	Default 
	{
		Renderstyle "Add";
		Decal "VinegaroonCrack";
		DamageType "Piercing";
		
		Scale 1.0;
	
		+NOINTERACTION;
		+PUFFONACTORS;
		+ALWAYSPUFF;
		+EXTREMEDEATH;
		+MTHRUSPECIES;
	}
	
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay;
		Death:
		Crash:
		XDeath:
			BTPF B 1 Bright
			{
				A_Explode(256, 256, XF_HURTSOURCE | XF_EXPLICITDAMAGETYPE, damagetype: "Fire");
				A_StartSound("EXPLOD2", 52, CHANF_DEFAULT, 1.0, 0.35);
				A_QuakeEX(6.0, 6.0, 6.0, 175, 0, 1028, flags: QF_SCALEDOWN);
				A_SetScale(Scale.X + 2.250);
				
				for (int i = 0; i < 55; ++i)
					A_SpawnItemEx("BurningMetal", 0, 0, 8, frandom(-30, 30), frandom(-30, 30), frandom(-30, 30));
				
				for (int i = 0; i < 16; ++i)
					A_SpawnItemEx("HEExplosion", frandom(-32, 32), frandom(-32, 32), frandom(0, 48));
				for (int i = 0; i < 16; ++i)
					A_SpawnItemEx("HEExplosion", frandom(-32, 32), frandom(-32, 32), frandom(-48, -0));
				for (int i = 0; i < 6; ++i)
					A_SpawnProjectile("ExplosionTailSpawn", 0, 0,  frandom(0, 360), CMF_AIMDIRECTION, frandom(-55, -5));
			}
			BTPF B 2 Bright
			{
				A_SetScale(Scale.X + 2.25);
				A_FadeOut(0.4);
				for (int i = 0; i < 16; ++i)
					A_SpawnItemEx("HEExplosion", frandom(-64, 64), frandom(-64, 64), frandom(0, 72));
				for (int i = 0; i < 16; ++i)
					A_SpawnItemEx("HEExplosion", frandom(-64, 64), frandom(-64, 64), frandom(-72, 0));
			}
			SPRK A 2 Bright
			{
				A_SetScale(Scale.X + 2.250);
				A_FadeOut(0.4);
				for (int i = 0; i < 16; ++i)
					A_SpawnItemEx("HEExplosion", frandom(-128, 128), frandom(-128, 128), frandom(0, 96));
				for (int i = 0; i < 16; ++i)
					A_SpawnItemEx("HEExplosion", frandom(-128, 128), frandom(-128, 128), frandom(-96, 0));
			}
			SPKO CCCCC 1 Bright A_FadeOut(0.2);
			TNT1 A 350;
			Stop;
	}
}

class VinegaroonFirstPuff : Actor 
{
	Default 
	{
		DamageType "Piercing";
		
		+PIERCEARMOR;
		+EXTREMEDEATH;
		+NOINTERACTION;
		+MTHRUSPECIES;
	}
	
	States 
	{
		Spawn:
		Death:
			TNT1 A 1;
			Stop;
	}
}

class BurningMetal : Actor 
{
	Default 
	{
		Projectile;
		
		DamageType "Piercing";
		Renderstyle "Add";
		
		Height 4;
		Radius 2;
		DamageFunction 5 * random(1, 3);
		Speed 0;
		
		Scale 0.065;
		
		+NODAMAGETHRUST;
		-NOGRAVITY;
		+BRIGHT;
	}
	
	States
	{
		Spawn:
			SPKO C 1;
			Loop;
			
		Death:
			Stop;
	}
}

class HasDoubleVinegaroon : Inventory
{
	Default 
	{
		inventory.Amount 1;
		Inventory.MaxAmount 1;
	}
}

class VinegaroonSpawn : CustomInventory replaces RocketLauncher
{
	Default
	{
		inventory.PickupMessage "Vinegaroon Slug Cannon installed";
		Inventory.PickUpSound "";
	}

	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HOD_Vinegaroon", 1, "SpawnAmmo", AAPTR_PLAYER1);
			PRXP A 1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODMainAmmo");
			Stop;
			
		SpawnTwinlinked:
			TNT1 A 0 A_JumpIf(bDropped == TRUE, "SpawnAmmo");
			TNT1 A 0 A_SpawnItemEx("OtherVinegaroonSpawn");
			Stop;
			
		Pickup:
			TNT1 A 0 
			{
				A_GiveInventory("HOD_Vinegaroon", 1);
				A_GiveInventory("HODMainAmmo", 30);
				A_StartSound("WEPUP", CHAN_AUTO);
			}
			Stop;
	}
}

class OtherVinegaroonSpawn : CustomInventory
{
	Default
	{
		inventory.PickupMessage "Twin-linked Vinegaroon installed";
		inventory.PickUpSound "DOUBUP";
	}

	States 
	{
		Spawn:
			//TNT1 A 0 NoDelay A_JumpIf(bDropped == TRUE, "SpawnAmmo");
			NLMG F -1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODMainAmmo");
			Stop;
			
		Pickup:
			TNT1 A 0 
			{
				A_GiveInventory("HasDoubleVinegaroon", 1);
				A_GiveInventory("HODMainAmmo", 30);
				A_StartSound("DOUBUP", CHAN_AUTO);
			}
			Stop;
	}
}
