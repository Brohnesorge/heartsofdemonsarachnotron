class HOD_Birdeater : ArachnoBaseWeapon
{
	Default
	{
		Tag "Birdeater Plasma Repeater";
		ArachnoBaseWeapon.ShortTag "Birdeater";
		Weapon.SlotNumber 	2;
		Weapon.AmmoType 	"HODMainAmmo";
		Weapon.AmmoUse		1;
	}
	
	States
	{
		WeaponReady:
			REPG A 1 A_JumpIfInventory("HasDoubleBirdeater", 1, "DoubleWeaponReady");
			REPG A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponFire");
			Loop;
			
		DoubleWeaponReady:
			2EPG A 1 A_Overlay(4, "OtherWeapon");
			REPG A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponDoubleFire");
			Loop;
			
		OtherWeapon:
			3EPG A 1;
			Loop;
			
		WeaponIn:
			TNT1 A 0 
			{
				A_OverlayOffset(-3, 0, 150);
				A_OverlayOffset(4, 0, 150);
			}
			REPG AAAAA 1 
			{
				A_OverlayOffset(-3, 0, -30, WOF_ADD);
				A_OverlayOffset(4, 0, -30, WOF_ADD);
			}
			Goto WeaponReady;
			
		WeaponOut:
			REPG AAA 1 
			{
				A_OverlayOffset(-3, 0, 30, WOF_ADD);
				A_OverlayOffset(4, 0, 30, WOF_ADD);
			}
			Stop;
			
		WeaponFire:
			TNT1 A 0 A_JumpIfNoAmmo("WeakWeaponFire");
			REPG A 1 Bright 
			{
				A_FireProjectile("BirdeaterBall", 0, TRUE, 0, -3);
				A_StartSound("Weapons/Birdeater",  CHAN_WEAPON);
				A_AlertMonsters();
			}
			REPG ABCD 1;
			Goto WeaponReady;
			
		WeakWeaponFire:
			REPG A 1 Bright 
			{
				A_FireProjectile("BirdeaterBall", 0, FALSE, 0, -3);
				A_StartSound("Weapons/Birdeater",  CHAN_WEAPON);
				A_AlertMonsters();
			}
			REPG ABCDABCDABCD 1;
			Goto WeaponReady;
			
		WeaponDoubleFire:
			TNT1 A 0 A_JumpIfNoAmmo("WeakWeaponDoubleFire");
			2EPG A 1 Bright 
			{
				A_FireProjectile("BirdeaterBallTwin" , 0, TRUE, -6, -3);
				A_StartSound("Weapons/Birdeater",  CHAN_WEAPON);
				A_Alertmonsters();
			}
			2EPG AB 1;
			2EPG C 1 A_Overlay(4, "OtherFire");
			2EPG D 1;
			2EPG AA 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponDoubleFire");
			Goto DoubleWeaponReady;
			
		WeakWeaponDoubleFire:
			2EPG A 1 Bright 
			{
				A_FireProjectile("BirdeaterBallTwin" , 0, TRUE, -6, -3);
				A_StartSound("Weapons/Birdeater",  CHAN_WEAPON);
				A_Alertmonsters();
			}
			2EPG AB 4;
			2EPG C 4 A_Overlay(4, "OtherFire");
			2EPG D 4;
			2EPG AA 1 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_ATTACK, "WeaponDoubleFire");
			Goto DoubleWeaponReady;
			
		OtherFire:
			3EPG A 1 Bright 
			{
				A_FireProjectile("BirdeaterBallTwin", 0, FALSE, 6, -3);
				A_StartSound("Weapons/Birdeater",  22);
				A_AlertMonsters();
			}
			3EPG AB 1;
			3EPG C 1;
			3EPG D 1;
			Goto OtherWeapon;
	}
}

class BirdeaterBall : FastProjectile
{
	Default 
	{
		Radius 4;
		Height 8;
		Speed 145;
		DamageFunction 5 * random(4, 6);
		Scale 0.2;
		Alpha 1.0;
		
		Renderstyle "Add";
		DamageType "Plasma";
		Decal "PlasmaScorch";
		
		+BRIGHT
		+BLOODSPLATTER;
		+MTHRUSPECIES;
	}
	
	States
	{
		Spawn:
			VFXH G 1;
			Loop;
			
		Death:
		Crash:
			TNT1 A 0
			{
				if (HODA_Smoke) A_SpawnItemEx("HODA_DebrisSmoke", zvel: frandom(2, 4)); 
			}
			
		XDeath:	
		
			VFXH C 1
			{
				for (int i = 0; i < 4; ++i)
					A_SpawnItemEx("YellowPlasmaSpark", xvel: frandom(-6, 0), yvel: frandom(-6, 6), zvel: frandom(-6, 6));
			}
			VFXH DEF 1 Bright A_SetScale(Scale.X * 1.25);
			Stop;
	}
}

class BirdeaterBallTwin : BirdeaterBall
{
	Default 
	{
		DamageFunction (5 * random(4, 6)) * 0.66;
	}
}

class YellowPlasmaSpark : Actor 
{
	Default 
	{
		Scale 0.05;
		Alpha 0.8;
	
		Renderstyle "Add";
	
		+NOINTERACTION;
		+BRIGHT;
	}
	
	States 
	{
		Spawn:
			BTPF A random(0, 3) NoDelay;
			BTPF AAAA 1 A_FadeTo(0.0, 0.2, TRUE);
			Stop;
	}
}

class HasDoubleBirdeater : Inventory 
{
	default
	{
		inventory.MaxAmount 1;
		inventory.Amount 1;
	}
}

class OtherBirdeaterSpawn : CustomInventory replaces SuperShotgun
{
	Default
	{
		inventory.PickupMessage "Twin-linked Birdeater installed";
		inventory.PickUpSound "";
	}

	States 
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIf(bDropped == TRUE, "SpawnAmmo");
			TNT1 A 0 A_JumpIfInventory("HasDoubleBirdeater", 1, "SpawnAmmo", AAPTR_PLAYER1);
			REPG I 1;
			Loop;
			
		SpawnAmmo:
			TNT1 A 0 A_SpawnItemEx("HODMainAmmo");
			Stop;
			
			
		Pickup:
			TNT1 A 0 
			{
				A_GiveInventory("HasDoubleBirdeater", 1);
				A_GiveInventory("HODMainAmmo", 30);
				A_StartSound("DOUBUP", CHAN_AUTO);
			}
			Stop;
	}
}