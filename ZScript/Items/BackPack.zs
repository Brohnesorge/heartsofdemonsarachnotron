class ArachnoBackPack : CustomInventory
{
	action void A_ExpandDong()
	{
		let plr = TheArachnotron(self);
		if (plr)
		{
			if (HODMainAmmo(plr.FindInventory("HODMainAmmo"))) { HODMainAmmo(plr.FindInventory("HODMainAmmo")).MaxAmount += 100; }
			if (HODSubAmmo(plr.FindInventory("HODSubAmmo"))) { HODSubAmmo(plr.FindInventory("HODSubAmmo")).MaxAmount += 10; }
		}
		A_GiveInventory("HODMainAmmo", 30);
		A_GiveInventory("HODSubAmmo", 10);
	}
	
	Default
	{
		Inventory.PickUpMessage "Extra Ammo Stores installed";
		Inventory.PickUpSound "BACKPAK";
	}
	
	States
	{
		Spawn:
			BPAK A -1;
			Stop;
			
		PickUp:
			TNT1 A 0 A_ExpandDong();
			Stop;
	}
}

class ArachnoSoulGem : CustomInventory 
{
	action void A_ExpandDong()
	{
		let plr = TheArachnotron(self);
		if (plr)
		{
			if (HODPP(plr.FindInventory("HODPP"))) { HODPP(plr.FindInventory("HODPP")).MaxAmount += 25; }
		}
		A_GiveInventory("HODPP", 25);
	}
	
	Default
	{
		Inventory.PickUpMessage "Soul Gem Installed";
		Inventory.PickUpSound "GEMGET";
		
		FloatBobStrength 0.5;
		FloatBobPhase 3;
		
		Scale 0.20;
		
		+COUNTITEM;
		+FLOATBOB;
		+DONTGIB;
		+BRIGHT;
		+INVENTORY.ALWAYSPICKUP;
	}
	
	States
	{
		Spawn:
			SGEM ABCDEFGHIJ 1;
			LOOP;
			
		PickUp:
			TNT1 A 0 A_ExpandDong();
			Stop;
	}
}

class BackPackSpawner : RandomSpawner replaces BackPack 
{
	Default 
	{
		DropItem "ArachnoBackPack", 255, 1;
		DropItem "ArachnoSoulGem", 255, 1;
	}
}