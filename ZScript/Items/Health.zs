class NuMedikit : Medikit replaces Medikit 
{
	Default 
	{
		Inventory.PickupMessage "Health Kit Administered";
			Inventory.PickUpSound "HealthGet";
		
		Scale 0.5;
	}
}

class NuStimPack : Stimpack replaces Stimpack 
{
	Default 
	{
		Inventory.PickupMessage "Healing Stimulant Administered";
		Inventory.PickUpSound "HealthGet";
		
		Scale 0.5;
	}
}

class NuHealthBonus : Health replaces HealthBonus 
{
	Default
	{
		Inventory.Amount 1;
		Inventory.MaxAmount 999;
		Inventory.PickUpMessage "Health Potion Administered";
		Inventory.PickUpSound "PotionGet";
		Scale 0.5;
		
		+BRIGHT;
		+COUNTITEM;
		+INVENTORY.ALWAYSPICKUP;
	}
	
	States
	{
		Spawn:
			BON1 ABCDCB 4;
			Loop;
	}
}

class NoArmorBonus : NuHealthBonus replaces ArmorBonus {}

class SoulSphereHealth : Health 
{
	Default 
	{
		Inventory.Amount 100;
		Inventory.MaxAmount 999;
	}
}

class NuSoulSphere : CustomInventory replaces SoulSphere 
{
	Default 
	{
		Inventory.PickUpMessage "Soul sphere drained";
		Inventory.PickUpSound "OrbUp";
		Scale 0.1;
		
		+BRIGHT;
		+INVENTORY.ALWAYSPICKUP;
		+COUNTITEM;
	}
	
	States 
	{
		Spawn:
			SOUL E 3 A_SpawnItemEx("HODA_FireBlue", frandom(-3, 3), frandom(-3, 3), frandom(10, 16));
			Loop;
		
		PickUp:
			TNT1 A 0
			{
				A_GiveInventory("SoulSphereHealth", 1);
				A_GiveInventory("HODPP", 100);
			}
			Stop;
	}
}

class NuMegaSphereHealth : SoulSphereHealth
{
	Default 
	{
		Inventory.Amount 300;
	}
}

class NuMegaSphere : CustomInventory replaces Megasphere 
{
	action void A_ExpandDong()
	{
		let plr = TheArachnotron(self);
		if (plr)
		{
			if (HODPP(plr.FindInventory("HODPP"))) { HODPP(plr.FindInventory("HODPP")).MaxAmount += 100; }
		}
		A_GiveInventory("HODPP", 100);
		A_GiveInventory("NuMegaSphereHealth", 1);
		A_GiveInventory("SpiderBarrier", 1);
	}
	
	Default 
	{
		Scale 0.1;
		Inventory.PickUpMessage "Mega Sphere drained";
		Inventory.PickUpSound "MegasphereGet";
		
		+Bright;
		+Inventory.AlwaysPickUp;
		+COUNTITEM;
	}
	
	States 
	{
		Spawn:
			SOUL G 5
			{
				A_SpawnItemEx("HODA_Fire", frandom(-3, 3), frandom(-3, 3), frandom(10, 16));
				A_SpawnItemEx("HODA_FireBlue", frandom(-3, 3), frandom(-3, 3), frandom(10, 16));
				A_SpawnItemEx("HODA_FireGreen", frandom(-3, 3), frandom(-3, 3), frandom(10, 16));
			}
			Loop;
			
		Pickup:
			TNT1 A 0 A_ExpandDong();
			Stop;
	}
}