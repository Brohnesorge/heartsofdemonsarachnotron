class HOD_MegaBarrier : CustomInventory replaces Berserk 
{
	Default 
	{
		Scale 1.0;
		
		Inventory.PickUpMessage "Megabarrier Obtained";
	}
	
	States
	{
		Spawn:
			BARR ABCDEEDCBA 4 Bright;
			Loop;
			
		PickUp:
			TNT1 A 0 A_GiveInventory("MegaResist", 1);
			Stop;
	}
}

class MegaResist : Inventory
{
	Default
	{
		Inventory.MaxAmount 1;
	}
}

class MindBooster : CustomInventory replaces Blursphere
{
	Default 
	{
		Inventory.PickUpMessage "Psi Amplifier Obtained";
	}
	
	States 
	{
		Spawn:
			MIND A -1;
			Stop; 
			
		Pickup:
			TNT1 A 0 
			{
				let plr = TheArachnotron(self);
			
				A_GiveInventory("HODPP", 999);
				A_StartSound("AMPGET", CHAN_AUTO);
				if(plr.PsiLevel < 10) 
				{
					plr.PsiLevel += 1;
					A_Print("Psychic Power Increased");
				}
			}
			Stop;
	}
}

class PowerMindStrength : PowerStrength 
{
	Default 
	{
		Powerup.Color "Cyan", 0.0;
	}
}

class NewInvul : CustomInventory replaces InvulnerabilitySphere
{
	Default
	{
		Scale 0.1;
		
		Inventory.PickupMessage "Invulnerability Sphere Obtained";
	}
	
	States 
	{
		Spawn:
			SOUL F 3 Bright A_SpawnItemEx("HODA_FireGreen", frandom(-3, 3), frandom(-3, 3), frandom(10, 16));
			Loop;
			
		Pickup:
			TNT1 A 0
			{
				let plr = TheArachnotron(self);
				
				plr.InvulnTimer = 35 * 45;
			}
			Stop;
	}
}