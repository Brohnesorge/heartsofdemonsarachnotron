class GreenFocusCrystal : CustomInventory replaces GreenArmor 
{
	Default 
	{
		Inventory.PickUpMessage "Focus Crystal Obtained \nQuality : Lesser";
		Inventory.PickUpSound "FocusGet";
		
		Scale 0.35;
		
		+Bright;
	}
	
	States
	{
		Spawn:
			TBGR AAAABCDEFEDCB 4;
			Loop;
			
		PickUp:
			TNT1 A 0
			{
				let plr = TheArachnotron(self);
				
				plr.PsiCryTimer += 35 * 60;
			}
			Stop;
	}
}

class BlueFocusCrystal : CustomInventory replaces BlueArmor
{
	Default 
	{
		Inventory.PickUpMessage "Focus Crystal Obtained \nQuality : Greater";
		Inventory.PickUpSound "FocusGet";
		
		XScale 0.65;
		YScale 0.40;
		
		+Bright;
	}
	
	States
	{
		Spawn:
			TBCY AAAABCDEFEDCB 2;
			Loop;
			
		PickUp:
			TNT1 A 0
			{
				let plr = TheArachnotron(self);
				
				plr.PsiCryTimer += 35 * 300;
			}
			Stop;
	}
}

class FocusMiss : PowerUp
{
	Default 
	{
		Powerup.Duration -1;
		
		+INVENTORY.ADDITIVETIME;
	}
}