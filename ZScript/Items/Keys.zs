class HODA_Key : DoomKey 
{
	string GetMSG;

	override void AttachToOwner(Actor user)
	{
		let plr = TheArachnotron(user);
	
		if(plr)
		{
			plr.A_Print(self.GetMSG);
		}
	
		Super.AttachToOwner(user);
	}

	Default 
	{
		Inventory.PickupMessage "";
	}
}

class HODA_Skull : DoomKey 
{
	string GetMSG;

	override void AttachToOwner(Actor user)
	{
		let plr = TheArachnotron(user);
	
		if(plr)
		{
			plr.A_Print(self.GetMSG);
		}
	
		Super.AttachToOwner(user);
	}

	Default
	{
		Scale 0.45;
	
		FloatBobStrength 0.25;
		FloatBobPhase	 10;	
		
		Inventory.PickupMessage "";
		Inventory.PickupSound	"SPOOK";
		
		+FLOATBOB;
	}
}

class HODA_RedKey : HODA_Key replaces RedCard 
{
	Default 
	{	
		Species "RedCard";
	}	
	
	States { Spawn: RKII AB 8 NoDelay { self.GetMSG = "\c[AraRed]red keycard obtained"; } Loop; }
}

class HODA_BlueKey : HODA_Key replaces BlueCard 
{
	Default 
	{
		Species "BlueCard";
	}
	
	States { Spawn: BKII AB 8 NoDelay { self.GetMSG = "\c[AraBlue]blue keycard obtained"; } Loop; }
}

class HODA_YellowKey : HODA_Key replaces YellowCard 
{
	Default 
	{
		Species "YellowCard";
	}
	
	States { Spawn: YKII AB 8 NoDelay { self.GetMSG = "\c[AraYellow]yellow keycard obtained"; } Loop; }
}

class HODA_RedSkull : HODA_Skull replaces RedSkull
{
	Default
	{
		Species "RedSkull";
	}
	
	States 
	{ 
		Spawn: 
			KULR ABCDEFGHIJKLMNOP 3 NoDelay { self.GetMSG = "\c[AraRuby]ruby ritual skull obtained"; } 
			Loop; 
	}
}

class HODA_BlueSkull : HODA_Skull replaces BlueSkull
{
	Default
	{
		Species "BlueSkull";
	}
	
	States 
	{ 
		Spawn: 
			KULB ABCDEFGHIJKLMNOP 3 NoDelay { self.GetMSG = "\c[AraSaph]sapphire ritual skull obtained"; } 
			Loop; 
	}
}

class HODA_YellowSkull : HODA_Skull replaces YellowSkull
{
	Default
	{
		Species "YellowSkull";
	}
	
	States 
	{ 
		Spawn: 
			KULY ABCDEFGHIJKLMNOP 3 NoDelay { self.GetMSG = "\c[AraGold]topaz ritual skull obtained"; } 
			Loop; 
	}
}