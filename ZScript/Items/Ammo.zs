class HODPP : Ammo 
{
	Default 
	{
		Inventory.Amount 10;
		Inventory.MaxAmount 100;
	}
}

class HODSoul : CustomInventory
{
	Default 
	{
		Inventory.PickUpMessage "Soul Absorbed";
		Inventory.PickupSound "SOULGET";
		Renderstyle "Add";
		Scale 0.25;
		+BRIGHT
	}
	
	States 
	{
		Spawn:
			BRMA ABCDEFGHIJKLMN 3;
			Loop;
			
		PickUp:
			TNT1 A 0
			{
				A_GiveInventory("HODPP", 5);
				HealThing(1);
			}
			Stop;
	}
}

class HODMainAmmo : Ammo replaces Clip
{
	Default 
	{
		Inventory.PickupMessage "Primary Weapon Ammo Obtained";
		Inventory.PickUpSound "AmmoUP";
		Inventory.Amount 10;
		Inventory.MaxAmount 300;
	}
	
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				if (!random(0, 3))
				{
					A_SpawnItemEX("HODSubAmmo", 0, 0, 0, random(-1, 1), random(-1, 1), random(0, 1));
				}
			}
		SitThere:
			WAMC ABB 4;
			Loop;
	}
}

class MainOverShells : HODMainAmmo replaces Shell {}
class MainOverRocket : HODMainAmmo replaces RocketAmmo {}
class MainOverCells : HODMainAmmo replaces Cell {}

class HODMainAmmoBig : HODMainAmmo replaces ClipBox
{
	Default 
	{
		Inventory.Amount 30;
	}
	
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				if (!random(0, 19))
				{
					A_SpawnItemEX("SataeSpawn", 0, 0, 0, random(-1, 1), random(-1, 1), random(0, 1));
				}
			}
		SitThere:
			WAMC CDD 4;
			Loop;
	}
}

class MainOverShellsBig : HODMainAmmoBig replaces ShellBox {}
class MainOverRocketBig : HODMainAmmoBig replaces RocketBox {}
class MainOverCellsBig : HODMainAmmoBig replaces CellPack {}

class HODSubAmmo : Ammo 
{
	Default 
	{
		Inventory.PickupMessage "Auxiliary Weapon Ammo Obtained";
		Inventory.PickUpSound "SMMOUP";
		Inventory.Amount 5;
		Inventory.MaxAmount 50;
	}
	
	States 
	{
		Spawn:
			AMOK A -1;
			Stop;
	}
}