class TheArachnotron : DoomPlayer
{
	int PowerReady;
	int SubReady;
	
	int PsiLevel;
	int PPTicker;
	int PPTarget; //vagene
	int PsyCharge;
	bool isFocusing;
	double CooldownTime;
	
	bool isSubArming;
	
	int QolShieldHealth;
	bool QolShieldActive;
	bool QolShieldDisrupted;
	
	int PsiCryTimer;
	int InvulnTimer;

	override void tick()
	{
		if (player && player.mo && player.mo == self)
		{
			if(!isFocusing)
			{
				if(CooldownTime <= 0) PPTicker ++;
				if(PsiCryTimer > 0) PPTicker ++;
			}
			
			if(PPTicker >= PPTarget) 
			{
				A_GiveInventory("HODPP", 1);
				PPTicker = 0;
			}
			
			if(CooldownTime > 0 && !isFocusing)
			{
				CooldownTime -= 1 + clamp(PsiLevel * 0.25, 0.0, 2.5);
			}
			
			if(CooldownTime < 0) CooldownTime = 0;
			
			if(PsiCryTimer) PsiCryTimer --;
			if(InvulnTimer) 
			{
				InvulnTimer --;
				bNODAMAGE = TRUE;
			}
			else { bNODAMAGE = FALSE; }
		
			if (CheckInventory("BaronEyes", 1))
			{
				shader.SetUniform1f(player, "NightVision", "exposure", 5);
				Shader.SetEnabled(player, "NightVision", TRUE);
			}
			else Shader.SetEnabled(player, "NightVision", FALSE);
		}
		
		super.tick();
	}
	
	override void BeginPlay()
	{
		PowerReady	= 0;
		SubReady	= 0;
		
		PsiLevel	= 0;
		PPTicker	= 0;
		PPTarget	= 70;
		isFocusing	= FALSE;
		
		QolShieldHealth 	= 0;
		QolShieldActive 	= FALSE;
		QolShieldDisrupted	= FALSE;
		
		A_SpawnItemEx("QolBarrier", flags: SXF_SETMASTER); 
		
		super.BeginPlay();
	}
	
	Default
	{
		Health 	200;
		Speed	1;
		Height 	56;
		Radius 	16;
		Mass 	2000;
		MaxStepHeight	 36;
		MaxDropOffHeight 48;
		
		DamageFactor "Magic",    0.2;
		DamageFactor "Fire",     0.5;
		DamageFactor "Ice",      0.5;
		DamageFactor "Electric", 0.5;
		DamageFactor "Melee",    2.0;
		
		Species "TheArachnotron";
		
		Player.MaxHealth	200;
		Player.ForwardMove 	0.85, 1.25;
		Player.SideMove 	0.75, 1.25;
		
		Player.DisplayName "Qol";
		Player.Soundclass "ArachnoPlayerSound";
		
		Player.StartItem "HOD_Birdeater", 1;
		Player.StartItem "HODMainAmmo", 300;
		Player.StartItem "HODPP", 100;
		Player.StartItem "HODSubAmmo", 50;
		Player.StartItem "Baronvision", 1;
		Player.StartItem "BaronResistance", 1;
		
		+THRUSPECIES;
	}
	
	States
	{
		Spawn:
			BSPI AB 10;
			Loop;
			
		See:
			TNT1 A 0 
			{
				if(player.onground) { A_StartSound("Player/joints", 33, CHANF_DEFAULT, 0.25); }
			}
			BSPI BC 3 A_SetTics(clamp(4 - vel.xy.length() / 5, 1, 6));
			BSPI A 6 
			{
				if(player.onground) { A_StartSound("player/Steps", 34); }
			}
			Goto Spawn;
			
		Missile:
		Melee:
			BSPI GH 3;
			Goto Spawn;
			
		Pain:
			BSPI B 8 A_Pain();
			Goto Spawn;
			
		Death:
			BSPI J 3 A_PlayerScream();
			BSPI KLMNO 3;
			BSPI P -1;
			Stop;
			
		XDeath:
			Goto Death;
	}
}

class QolBarrier : Actor
{
	bool isDisrupted;

	override void Tick()
	{
		if(master)
		{
			A_Warp(AAPTR_MASTER);
			
			if(self.Health > 0)
			{
				TheArachnotron(master).QolShieldActive = TRUE;
				TheArachnotron(master).QolShieldHealth = self.health;
				
				if(self.isDisrupted) TheArachnotron(master).QolShieldDisrupted = TRUE;
				else TheArachnotron(master).QolShieldDisrupted = FALSE;
			}
		}
		
		if(!master || master.health < 1) { self.Destroy(); }
		
		Super.Tick();
	}

	Default 
	{
		Health 		300;
		Height      64;
		Radius      36; 
		
		XScale	2.0;
		YScale	1.5;
		Alpha	0.01;
		
		RenderStyle "Add";
		
		RadiusDamageFactor			0.75;
		DamageFactor "Magic",		0.50;
		DamageFactor "Fire",		1.00;
		DamageFactor "Ice",			1.00;
		DamageFactor "Electric",	1.00;
		DamageFactor "Melee",		1.00;
		DamageFactor "Plasma",		1.50;
		DamageFactor "SmallArms",	1.00;
		DamageFactor "LongArms",	0.50;
		DamageFactor "Bullet",		0.75;
		
		PainChance "Melee", 256;
	
		Species 	"TheArachnotron";
		BloodType 	"QolBarrierBlood";
		
		+BRIGHT;
		+BLOODLESSIMPACT;
		-BLOODSPLATTER;
		+SHOOTABLE;
		+THRUSPECIES;
		+STOPRAILS;
		+DONTRIP;
		+NOCLIP;
		+FRIENDLY;
		+CANTSEEK;
	}
	
	States 
	{
		Spawn:
			
		See:
			TNT1 A 2;
			Loop;
			
		Pain:
		Pain.Melee:	
			TNT1 A 74 
			{ 
				self.bSHOOTABLE = FALSE; 
				isDisrupted = TRUE; 
				for (int i = 0; i < 12; ++i)
					A_SpawnItemEx("QolBarrierWave", frandom(-36, 36), frandom(-36, 36), 24 * frandom(0.5, 2.0), frandom(0, 4), frandom(-6, 6), frandom(-6, 6));
			}
			TNT1 A 1 { self.bSHOOTABLE = TRUE; isDisrupted = FALSE; }
			Goto See;
		
		Death:
		XDeath:
			TNT1 A 0
			{
				A_StartSound("SHIDIE", CHAN_AUTO, CHANF_DEFAULT, 1.0, ATTN_NONE);
				
				if(master)
				{
					TheArachnotron(master).QolShieldHealth = 0;
					TheArachnotron(master).QolShieldActive = FALSE;
					TheArachnotron(master).QolShieldDisrupted = FALSE;
				}
			}
			TNT1 AAAAAAAA 1 
			{
				for (int i = 0; i < 12; ++i)
					A_SpawnItemEx("QolBarrierSpark", frandom(-36, 36), frandom(-36, 36), 24 * frandom(0.5, 2.0), frandom(0, 4), frandom(-6, 6), frandom(-6, 6));
			}
			
			Stop;
	}		
}

class QolMegaBarrier : QolBarrier
{
	override void Tick()
	{
		if(master)
		{
			A_Warp(AAPTR_MASTER);
			
			if(self.Health > 0)
			{
				TheArachnotron(master).QolShieldActive = TRUE;
				TheArachnotron(master).QolShieldHealth = self.health;
				
				if(self.isDisrupted) TheArachnotron(master).QolShieldDisrupted = TRUE;
				else TheArachnotron(master).QolShieldDisrupted = FALSE;
			}
		}
		
		if(self.Health > 300)
		{
			self.bREFLECTIVE = TRUE;
			self.bMIRRORREFLECT = TRUE;
		}
		else self.bREFLECTIVE = FALSE;
		
		if(!master || master.health < 1) { self.Destroy(); }
		
		Super.Tick();
	}

	Default 
	{
		Health 999;
		
		RadiusDamageFactor			0.50;
		DamageFactor "Magic",		0.50;
		DamageFactor "Fire",		0.75;
		DamageFactor "Ice",			0.75;
		DamageFactor "Electric",	0.75;
		DamageFactor "Melee",		0.75;
		DamageFactor "Plasma",		1.00;
		DamageFactor "SmallArms",	0.66;
		DamageFactor "LongArms",	0.50;
		DamageFactor "Bullet",		0.50;
		
		+NOPAIN;
	}
	
	States 
	{
		Death:
		XDeath:
			TNT1 A 0
			{
				A_StartSound("SHIDIE", CHAN_AUTO, CHANF_DEFAULT, 1.0, ATTN_NONE);
				A_Explode(256, 128, XF_NOTMISSILE | XF_THRUSTLESS | XF_NOALLIES, FALSE, 128, damagetype: "Plasma");
				if(master)
				{
					TheArachnotron(master).QolShieldHealth = 0;
					TheArachnotron(master).QolShieldActive = FALSE;
					TheArachnotron(master).QolShieldDisrupted = FALSE;
				}
			}
			TNT1 AAAAAAAA 1 
			{
				for (int i = 0; i < 12; ++i)
					A_SpawnItemEx("QolBarrierSpark", frandom(-36, 36), frandom(-36, 36), 24 * frandom(0.5, 2.0), frandom(0, 4), frandom(-6, 6), frandom(-6, 6));
				
				for (int i = 0; i < 18; ++i)
					A_SpawnItemEx("RecluseFlame", frandom(-48, 48), frandom(-48, 48), 24 * frandom(0.5, 2.0));
			}
			Stop;
	}
}

class QolBarrierBlood : Actor 
{
	Default 
	{
		Scale 0.25;
		Alpha 1.0;
		VSpeed 0;
	
		Renderstyle "Add";
	
		+NOINTERACTION;
		+Bright;
	}
	
	States 
	{
		Spawn:
			TNT1 A 1 NoDelay 
			{
				A_StartSound("Player/BarrierHit", CHAN_AUTO, CHANF_DEFAULT, 1.0, ATTN_NONE);
				A_SpawnItemEx("QolBarrierSpark");
				A_SpawnItemEx("QolBarrierWave");
			}
			Stop;
	}
}

class QolBarrierSpark : Actor 
{
	Default 
	{
		Scale 0.5;
		Alpha 1.0;
		
		RenderStyle "Add";
		
		+NOINTERACTION;
		+ROLLSPRITE;
		+ROLLCENTER;
		+BRIGHT;
	}
	
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				A_SetRoll(frandom(0, 360));
				bSPRITEFLIP = random(0, 1);
				A_SetScale(Scale.X * frandom(0.85, 1.15));
			}
			ZHIT ABCDE 1;
			Stop;
	}
}

class QolBarrierWave : QolBarrierSpark
{
	Default 
	{
		Alpha 0.33;
	}

	States 
	{
		Spawn:
			ZH1T ABCDE 1 NoDelay;
			Stop;
	}
}

class SpiderBarrier : GreenArmor
{
	Default 
	{
		Armor.SavePercent 	100;
		Armor.SaveAmount 	300;
		
		DamageFactor "Normal", 0.5;
	}
}

class SpiderBarrierHeal : ArmorBonus
{
	Default
	{
		Armor.SaveAmount 	1;
		Armor.MaxSaveAmount 300;
		Armor.SavePercent 	100;
		
		DamageFactor "Normal", 0.5;
	}
}

class DummiedOut : Inventory 
{
	Default 
	{
		inventory.amount 0;
		inventory.maxamount 0;
	}
	
	States
	{
		Spawn:
			TNT1 A 0 NoDelay;
			Stop;
	}
}

class NoPISS : DummiedOut replaces Pistol {} //fucking why do people keep placing fucking PISTOLS
class NoCS : DummiedOut replaces Chainsaw {}
class NoIR : DummiedOut replaces Infrared {}
class NoRS : DummiedOut replaces Radsuit {}

class BaronVision : CustomInventory
{
	Default
	{
		Inventory.MaxAmount 1;
		-INVENTORY.INVBAR
	}

	States
	{
		Spawn:
			TNT1 A -1;
			Stop;

		Use:
			TNT1 A 0 A_JumpIfInventory ("BaronEyes", 1, "Use2");
			TNT1 A 0 
			{
				A_GiveInventory ("BaronEyes");
				A_StartSound("NVSTART", 110);
				
			}
			TNT1 A 1;
			Fail;

		Use2:
			TNT1 A 0
			{
				A_TakeInventory ("BaronEyes");
			}
			TNT1 A 1;
			Fail;
	}
}

class BaronEyes : PowerLightAmp
{
	Default
	{
		Powerup.Duration 0x7FFFFFFF;
		Powerup.Colormap 1.0, 1.0, 1.0;
	}
}


class BaronResistance : PowerIronFeet
{
	Default
	{
		Powerup.Duration 0x7FFFFFFF;
		Powerup.Color "", 0;
		+INVENTORY.PERSISTENTPOWER
		+INVENTORY.UNDROPPABLE
		+INVENTORY.AUTOACTIVATE
	}
}


