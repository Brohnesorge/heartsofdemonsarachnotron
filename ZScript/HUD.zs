class ArachnoHUD : BaseStatusBar
{
	HUDFont mBigFont;
	HUDFont mSmallFont;
	HUDFont mBColorFont;


	override void Init()
	{
		super.Init();
		SetSize(0, 320, 240);
		
		Font fnt;
		Font ffnt;

		//Defines default Doom fonts for use in the HUD.
		fnt = "revbig";
		mBigFont = HUDFont.Create(fnt);

		ffnt = "revsmall";
		mSmallFont = HUDFont.Create(ffnt);

	}
	
	override void Draw (int state, double TicFrac)
	{
		Super.Draw(state, TicFrac);
		
		if (state == HUD_Fullscreen)
        {
            BeginHUD();
            DrawFullScreenStuff(); //You can rename this, but there's no need. Personally, I don't bother renaming the base stuff.
        }
	}
	
	void DrawFullScreenStuff() 
	{
		let plr 	= TheArachnotron(CPlayer.mo);
		let armor 	= Cplayer.mo.FindInventory("BasicArmor");
		let pp 		= GetAmount("HODPP");
		let wpn		= CPlayer.ReadyWeapon;
		let base 	= ArachnoBaseWeapon(wpn);
		let Main 	= GetAmount("HODMainAmmo");
		let Sub 	= GetAmount("HODSubAmmo");
		let MainCent = GetAmount("HODMainAmmo")/double(GetMaxAmount("HODMainAmmo"));
		
		DrawImage("StatusBl", (10, -10), DI_SCREEN_LEFT_BOTTOM | DI_ITEM_LEFT_BOTTOM, 1.0);
	
		if(plr.Health > 0) 
		{ 
			DrawString(mBigFont, FormatNumber(plr.Health, 3, 3, 2), (30, -63), DI_SCREEN_LEFT_BOTTOM | DI_TEXT_ALIGN_LEFT); 
			if(plr.Health < 50) DrawString(mBigFont, "\c[AraRed]"..FormatNumber(plr.Health, 3, 3, 2), (30, -63), DI_SCREEN_LEFT_BOTTOM | DI_TEXT_ALIGN_LEFT); 
		}
		else { DrawString(mBigFont, "\c[AraBlack]000", (30, -63), DI_SCREEN_LEFT_BOTTOM | DI_TEXT_ALIGN_LEFT); }
		
		if(plr.QolShieldHealth > 0) 
		{
			DrawString(mBigFont, FormatNumber(plr.QolShieldHealth, 3, 3, 2), (30, -45), DI_SCREEN_LEFT_BOTTOM | DI_TEXT_ALIGN_LEFT); 
			
			if(plr.QolShieldHealth > 300) 
			{ DrawString(mBigFont, "\c[AraGreen]"..FormatNumber(plr.QolShieldHealth, 3, 3, 2), (30, -45), DI_SCREEN_LEFT_BOTTOM | DI_TEXT_ALIGN_LEFT); }
			if(plr.QolShieldHealth < 100) 
			{ DrawString(mBigFont, "\c[AraOrange]"..FormatNumber(plr.QolShieldHealth, 3, 3, 2), (30, -45), DI_SCREEN_LEFT_BOTTOM | DI_TEXT_ALIGN_LEFT); }
		}
		else { DrawString(mBigFont, "\c[AraBlack]000", (30, -45), DI_SCREEN_LEFT_BOTTOM | DI_TEXT_ALIGN_LEFT); }
		
		if(pp >= base.psycost ) { DrawString(mBigFont, FormatNumber(pp, 3, 3, 2), (30, -27), DI_SCREEN_LEFT_BOTTOM | DI_TEXT_ALIGN_LEFT); }
		else { DrawString(mBigFont, "\c[AraOrange]"..FormatNumber(pp, 3, 3, 2), (30, -27), DI_SCREEN_LEFT_BOTTOM | DI_TEXT_ALIGN_LEFT); }
		
		
		DrawImage("CombatBl", (-10, -10), DI_SCREEN_RIGHT_BOTTOM | DI_ITEM_RIGHT_BOTTOM, 1.0);
		
		DrawString(mBigFont, FormatNumber(Main, 3, 3, 2), (-31, -27), DI_SCREEN_RIGHT_BOTTOM | DI_TEXT_ALIGN_RIGHT);
		if(MainCent < 0.20) DrawString(mBigFont, "\c[AraOrange]"..FormatNumber(Main, 3, 3, 2), (-31, -27), DI_SCREEN_RIGHT_BOTTOM | DI_TEXT_ALIGN_RIGHT);
		if(Main < 1) DrawString(mBigFont, "\c[AraRed]"..FormatNumber(Main, 3, 3, 2), (-31, -27), DI_SCREEN_RIGHT_BOTTOM | DI_TEXT_ALIGN_RIGHT);
		DrawString(mBigFont, FormatNumber(Sub, 3, 3, 2), (-31, -45), DI_SCREEN_RIGHT_BOTTOM | DI_TEXT_ALIGN_RIGHT);
		if(Sub < 10) DrawString(mBigFont, "\c[AraOrange]"..FormatNumber(Sub, 3, 3, 2), (-31, -45), DI_SCREEN_RIGHT_BOTTOM | DI_TEXT_ALIGN_RIGHT);
		if(Sub < base.subcost) DrawString(mBigFont, "\c[AraRed]"..FormatNumber(Sub, 3, 3, 2), (-31, -45), DI_SCREEN_RIGHT_BOTTOM | DI_TEXT_ALIGN_RIGHT);
		
		if (base)
		{
			DrawString(mSmallFont, base.ShortTag, (-14, -71), DI_SCREEN_RIGHT_BOTTOM | DI_TEXT_ALIGN_RIGHT);
		}
		
		DrawImage("BEYLINE", (0, 10), DI_SCREEN_HCENTER | DI_ITEM_TOP, 1.0);
		if (CheckInventory("HODA_BlueKey"))		DrawImage("BLUKEYC", (0, 10), DI_SCREEN_HCENTER | DI_ITEM_TOP, 0.75);
		if (CheckInventory("HODA_YellowKey"))	DrawImage("YELKEYC", (0, 10), DI_SCREEN_HCENTER | DI_ITEM_TOP, 0.75);
		if (CheckInventory("HODA_RedKey"))		DrawImage("REDKEYC", (0, 10), DI_SCREEN_HCENTER | DI_ITEM_TOP, 0.75);
		if (CheckInventory("HODA_BlueSkull"))	DrawImage("BLUKEYS", (0, 10), DI_SCREEN_HCENTER | DI_ITEM_TOP, 0.75);
		if (CheckInventory("HODA_YellowSkull"))	DrawImage("YELKEYS", (0, 10), DI_SCREEN_HCENTER | DI_ITEM_TOP, 0.75);
		if (CheckInventory("HODA_RedSkull"))	DrawImage("REDKEYS", (0, 10), DI_SCREEN_HCENTER | DI_ITEM_TOP, 0.75);
		
		DrawImage("BENTLINE", (0, 1), DI_SCREEN_CENTER | DI_ITEM_CENTER, 1.0, (-1, -1), (0.5, 0.5));
		Drawimage("BYDCROSS", (0, 0), DI_SCREEN_CENTER | DI_ITEM_CENTeR, 0.50, (-1, -1), (0.5, 0.5));
		
		if(plr.Health > 0)
		{
			if(!plr.QolShieldActive) 
			{
				DrawString(mBigFont, "DANGER", (0, 90), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, font.CR_RED);
				DrawString(mSmallFont, "Psionic Barrier Destroyed", (0, 106), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER);
			}
			if(plr.QolShieldDisrupted) 
			{
				DrawString(mBigFont, "CAUTION", (0, 90), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, font.CR_YELLOW);
				DrawString(mSmallFont, "Psionic Barrier Disrupted", (0, 106), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER);
			}
			if(!Main)
			{
				DrawString(mBigFont, "Warning", (0, 116), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, font.CR_ORANGE);
				DrawString(mSmallFont, "Primary Weapon Ammo Depleted", (0, 132), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER);
			}
			if(!Sub)
			{
				DrawString(mBigFont, "Warning", (0, 142), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, font.CR_ORANGE);
				DrawString(mSmallFont, "Auxiliary Weapon Ammo Depleted", (0, 158), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER);
			}
			
			if(plr.InvulnTimer)
			{
				DrawString(mSmallFont, "Invuln: "..FormatNumber(plr.InvulnTimer/double(35)).." secs", (10, 100), DI_SCREEN_LEFT_TOP | DI_TEXT_ALIGN_LEFT);
				Fill(Color(12, 245, 194, 66), 0, 0, 4000, 4000, DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP);
			}
			if(plr.PsiCryTimer)
			{
				DrawString(mSmallFont, "Focus Crystal: "..FormatNumber(plr.PsiCryTimer/double(35)).." secs", (10, 110), DI_SCREEN_LEFT_TOP | DI_TEXT_ALIGN_LEFT);
			}
			if(CheckInventory("MegaResist", 1))
			{
				DrawString(mSmallFont, "Megabarrier Queued", (10, 120), DI_SCREEN_LEFT_TOP | DI_TEXT_ALIGN_LEFT);
			}
			
				int texYpos = 64;
			int SubInc = -25;
			int SubtexYpos = texYpos + SubInc;
			int barYpos = texYPos + 16;
			int SubbarYpos = barYpos + SubInc;
			int barWide = 80;
			
			
			if(plr.isSubArming)
			{
				DrawString(mSmallFont, base.SubCharge >= base.SubArm ? base.SubReady.." Armed" : "Arming...", (0, SubtexYpos), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, base.SubCharge >= base.SubArm ? font.CR_GREEN : font.CR_WHITE);
				Fill(Color(64, 124, 229, 255), -barWide * 0.5, SubbarYpos, barWide, -2, DI_SCREEN_CENTER);
				Fill(Color(255, 255, 255, 255), -barWide * 0.5, SubbarYpos, barWide * clamp(base.SubCharge / double(base.SubArm), 0.0, 1.0), -2, DI_SCREEN_CENTER);
			}
			
			if(base.ScorpUp)
			{
				DrawString(mSmallFont, "Scorpion Charging", (0, SubtexYpos), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, font.CR_WHITE);
				Fill(Color(64, 124, 229, 255), -barWide * 0.5, SubbarYpos, barWide, -2, DI_SCREEN_CENTER);
				Fill(Color(255, 255, 255, 255), -barWide * 0.5, SubbarYpos, barWide * clamp(base.ScorpCharge / double(base.ScorpTarget), 0.0, 1.0), -2, DI_SCREEN_CENTER);
			}
			
			if(base.SubFiring)
			{
				DrawString(mSmallFont, base.SubReady.." Firing", (0, SubtexYpos), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, font.CR_WHITE);
				Fill(Color(64, 124, 229, 255), -barWide * 0.5, SubbarYpos, barWide, -2, DI_SCREEN_CENTER);
				Fill(Color(255, 255, 255, 255), -barWide * 0.5, SubbarYpos, barWide * clamp(base.SubAmt / double(base.SubTarget), 0.0, 1.0), -2, DI_SCREEN_CENTER);
			}
			
			if(plr.CooldownTime && !base.PsyCharge)
			{
				DrawString(mSmallFont, "Cooling Down", (0, texYpos), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, font.CR_YELLOW);
				Fill(Color(64, 124, 229, 255), -barWide * 0.5, barYpos, barWide, -2, DI_SCREEN_CENTER);
				Fill(Color(255, 240, 133, 19), -barWide * 0.5, barYpos, barWide * clamp(plr.CooldownTime / double(175), 0.0, 1.0), -2, DI_SCREEN_CENTER);
				Fill(Color(255, 252, 75, 10), -barWide * 0.5, barYpos, barWide * clamp((plr.CooldownTime - 175) / double(175), 0.0, 1.0), -2, DI_SCREEN_CENTER);
			}
			
			if(base.PsyCharge > 0)
			{
				DrawString(mSmallFont, base.PsyCharge >= base.FocusTarget ? "Psionics Ready" : "Focusing...", (0, texYpos), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, base.PsyCharge >= base.FocusTarget ? font.CR_GREEN : font.CR_WHITE);
				DrawString(mSmallFont, plr.CooldownTime > 0 ? "Caution: Overheated" : "", (0, texYpos - 7), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, font.CR_ORANGE);
				DrawString(mSmallFont, base.PsyCost > pp ? "Warning: Insufficient Psionic Energy" : "", (0, texYpos + 7), DI_SCREEN_CENTER | DI_TEXT_ALIGN_CENTER, font.CR_RED);
				
				Fill(Color(64, 124, 229, 255), -barWide * 0.5, barYpos, barWide, -2, DI_SCREEN_CENTER);
				Fill(Color(255, 255, 255, 255), -barWide * 0.5, barYpos, barWide * clamp(base.PsyCharge / double(base.FocusTarget), 0.0, 1.0), -2, DI_SCREEN_CENTER);

				if(base.PsyCharge >= base.FocusTarget)
				{
					Fill(Color(255, 229, 124, 255), -barWide * 0.5, barYpos, barWide, -2, DI_SCREEN_CENTER);
					
					if(plr.PowerReady == 0)
					{	
						Fill(Color(255, 3, 148, 252),	 -barWide * 0.5, barYpos, barWide * clamp((base.PsyCharge - 3) / double(17), 0.0, 1.0), -2, DI_SCREEN_CENTER);
						Fill(Color(255, 3, 252, 15),	 -barWide * 0.5, barYpos, barWide * clamp((base.PsyCharge - 20) / double(44), 0.0, 1.0), -2, DI_SCREEN_CENTER);
						Fill(Color(255, 255, 255, 0), 	 -barWide * 0.5, barYpos, barWide * clamp((base.PsyCharge - 60) / double(49), 0.0, 1.0), -2, DI_SCREEN_CENTER);
						Fill(Color(255, 255, 0, 0), 	 -barWide * 0.5, barYpos, barWide * clamp((base.PsyCharge - 105) / double(143), 0.0, 1.0), -2, DI_SCREEN_CENTER);
					}
				}
			}
		}
		
		Fill(Color(126, 124, 229, 255), -75, 66, 2, -130 * Clamp(plr.Health / double(plr.MaxHealth), 0.0, 1.0), DI_SCREEN_CENTER | DI_ITEM_CENTER);
		Fill(Color(126, 255, 255, 255), -77, 66, 2, -130 * clamp(plr.QolShieldHealth / double(300), 0.0, 1.0), DI_SCREEN_CENTER | DI_ITEM_CENTER);
		
		Fill(Color(126, 124, 229, 255), 73, 66, 2, -130 * clamp(Main / Double(300), 0.0, 1.0), DI_SCREEN_CENTER | DI_ITEM_CENTER);
		Fill(Color(126, 255, 255, 255), 75, 66, 2, -130 * clamp(Sub / Double(50), 0.0, 1.0), DI_SCREEN_CENTER | DI_ITEM_CENTER);
			
		DrawString(mSmallFont, base.PsyReady, (102, -2), DI_SCREEN_CENTER | DI_TEXT_ALIGN_LEFT | DI_ITEM_CENTER); 
		DrawString(mSmallFont, base.SubReady, (-102, -2), DI_SCREEN_CENTER | DI_TEXT_ALIGN_RIGHT | DI_ITEM_CENTER);
		
	
		
		int WepTexPosXBase = -40;
		int WepTexPosXInc = 20;
		
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(2) ? "2" : "", (WepTexPosXBase, -20), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(2) ? CheckInventory("HasDoubleBirdEater", 1) ? ".." : "." : "", (WepTexPosXBase, -15), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(3) ? "3" : "", (WepTexPosXBase + WepTexPosXInc, -20), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(3) ? CheckInventory("HasDoubleRecluse", 1) ? ".." :"." : "", (WepTexPosXBase + WepTexPosXInc, -15), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(4) ? "4" : "", (WepTexPosXBase + (WepTexPosXInc * 2), -20), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(4) ? CheckInventory("HasDoubleWidow", 1) ? ".." : "." : "", (WepTexPosXBase + (WepTexPosXInc * 2), -15), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(5) ? "5" : "", (WepTexPosXBase + (WepTexPosXInc * 3), -20), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(5) ? "." : "", (WepTexPosXBase + (WepTexPosXInc * 3), -15), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(6) ? "6" : "", (WepTexPosXBase + (WepTexPosXInc * 4), -20), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		DrawString(mSmallFont, CPlayer.HasWeaponsInSlot(6) ? CheckInventory("HasDoubleGoliath", 1) ? ".." : "." : "", (WepTexPosXBase + (WepTexPosXInc * 4), -15), DI_SCREEN_CENTER_BOTTOM | DI_TEXT_ALIGN_CENTER);
		
		DrawImage("PsionBL", (10, 0), DI_SCREEN_VCENTER | DI_SCREEN_LEFT | DI_ITEM_LEFT_TOP, 1.0);
		
		DrawImage("PsiSelBL", (10, -80), DI_SCREEN_LEFT_BOTTOM | DI_ITEM_LEFT_BOTTOM, 1.0);
		
		if (plr.PowerReady == 0) { DrawImage("PsiSel1", (10, -87), DI_SCREEN_LEFT_BOTTOM | DI_ITEM_LEFT_BOTTOM, 1.0);}
		if (plr.PowerReady == 1) { DrawImage("PsiSel2", (10, -87), DI_SCREEN_LEFT_BOTTOM | DI_ITEM_LEFT_BOTTOM, 1.0);}
		if (plr.PowerReady == 2) { DrawImage("PsiSel3", (10, -87), DI_SCREEN_LEFT_BOTTOM | DI_ITEM_LEFT_BOTTOM, 1.0);}
		if (plr.PowerReady == 3) { DrawImage("PsiSel4", (10, -87), DI_SCREEN_LEFT_BOTTOM | DI_ITEM_LEFT_BOTTOM, 1.0);}
		
		DrawImage("SubBL", (-10, 0), DI_SCREEN_VCENTER | DI_SCREEN_RIGHT | DI_ITEM_RIGHT_TOP, 1.0);
		
		DrawImage("SubSelBL", (-10, -80), DI_SCREEN_RIGHT_BOTTOM | DI_ITEM_RIGHT_BOTTOM, 1.0);
		
		if (plr.SubReady == 0) { DrawImage("SubCent1", (-12, -87), DI_SCREEN_RIGHT_BOTTOM | DI_ITEM_RIGHT_BOTTOM, 1.0); }
		if (plr.SubReady == 1) { DrawImage("SubCent2", (-12, -87), DI_SCREEN_RIGHT_BOTTOM | DI_ITEM_RIGHT_BOTTOM, 1.0); }
		if (plr.SubReady == 2) { DrawImage("SubCent3", (-12, -87), DI_SCREEN_RIGHT_BOTTOM | DI_ITEM_RIGHT_BOTTOM, 1.0); }
		
		if (CheckInventory("hasSatae")) { DrawImage("SubHas1", (-10, 0), DI_SCREEN_VCENTER | DI_SCREEN_RIGHT | DI_ITEM_RIGHT_TOP, 1.0); }
		if (CheckInventory("hasScorpion")) { DrawImage("SubHas2", (-10, 0), DI_SCREEN_VCENTER | DI_SCREEN_RIGHT | DI_ITEM_RIGHT_TOP, 1.0); }
		
		//Time.
		DrawString(mSmallFont, level.TimeFormatted(), (38, 16), DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP | DI_TEXT_ALIGN_LEFT);
		//Kills
		DrawString(mSmallFont, formatNumber(level.killed_monsters, 3, 3, 2), (38, 23), DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP | DI_TEXT_ALIGN_LEFT);
		DrawString(mSmallFont, formatNumber(level.total_monsters, 3, 3, 2), (93, 23), DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP | DI_TEXT_ALIGN_LEFT);
		//ITEMS
		DrawString(mSmallFont, formatNumber(level.found_items, 3, 3, 2), (38, 30), DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP | DI_TEXT_ALIGN_LEFT);
		DrawString(mSmallFont, formatNumber(level.total_items, 3, 3, 2), (93, 30), DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP | DI_TEXT_ALIGN_LEFT);
		//SECRETS
		DrawString(mSmallFont, formatNumber(level.found_secrets, 3, 3, 2), (38, 37), DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP | DI_TEXT_ALIGN_LEFT);
		DrawString(mSmallFont, formatNumber(level.total_secrets, 3, 3, 2), (93, 37), DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP | DI_TEXT_ALIGN_LEFT);

		DrawString(mSmallFont, level.levelname, (38, 44), DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP | DI_TEXT_ALIGN_LEFT);

		DrawImage("BNFOLINE", (10, 10), DI_SCREEN_LEFT_TOP | DI_ITEM_LEFT_TOP, 0.75);
	}
}