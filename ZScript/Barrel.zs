class DangOlBarrelo : ExplosiveBarrel replaces ExplosiveBarrel
{


	States
	{
		Spawn:
			BAR1 AB 6;
			Loop;
		 Death:
			BEXP A 5 Bright;
			BEXP B 5 Bright A_Scream;
			BEXP C 5 Bright;
		FancyDeath:
			TNT1 A 0
			{
				A_Explode(192, 192, XF_HURTSOURCE | XF_EXPLICITDAMAGETYPE, damagetype: "Fire");
				A_QuakeEX(0, 6.0, 6.0, 50, 0, 640, flags: QF_SCALEDOWN);
				A_StartSound("BarrelExplosion", 98, CHANF_DEFAULT, 1.0, 0.35);
				A_StartSound("C4BoomWave", 99, CHANF_DEFAULT, 1.0, 0.35);
				A_StartSound("C4FAR", 100, CHANF_DEFAULT, 1.0, ATTN_NONE);

			}
			TNT1 AAA 0 A_SpawnItemEx("HEExplosion", frandom(-35, 35), frandom(-35, 35), frandom(-10, 15), 0, frandom(-4, 4), frandom(0,2));
			TNT1 A 1 A_SpawnItemEx("HEExplosion", frandom(-35, 35), frandom(-35, 35), frandom(-10, 15), 0, frandom(-4, 4), frandom(0,2));
			TNT1 AAAA 0 A_SpawnItemEx("HEExplosion", frandom(-65, 65), frandom(-45, 45), frandom(15, 35), 0, frandom(-4, 4), frandom(0,2));
			TNT1 A 1 A_SpawnItemEx("HEExplosion", frandom(-65, 65), frandom(-45, 45), frandom(15, 35), 0, frandom(-4, 4), frandom(0,2));
			TNT1 AAAA 0 A_SpawnItemEx("HEExplosion", frandom(-85, 85), frandom(-85, 85), frandom(35, 55), 0, frandom(-4, 4), frandom(0,2));
			TNT1 A 1 A_SpawnItemEx("HEExplosion", frandom(-85, 85), frandom(-85, 85), frandom(35, 55), 0, frandom(-4, 4), frandom(0,2));
			TNT1 AAA 0 A_SpawnItemEx("HEExplosion", frandom(-45, 45), frandom(-45, 45), frandom(55, 75), 0, frandom(-4, 4), frandom(0,2));
			TNT1 A 1 A_SpawnItemEx("HEExplosion", frandom(-45, 45), frandom(-45, 45), frandom(55, 75), 0, frandom(-4, 4), frandom(0,2));
			TNT1 AAA 0 A_SpawnItemEx("HEExplosion", frandom(-25, 25), frandom(-25, 25), frandom(75, 95), 0, frandom(-4, 4), frandom(0,2));
			TNT1 A 1 A_SpawnItemEx("HEExplosion", frandom(-25, 25), frandom(-25, 25), frandom(75, 95), 0, frandom(-4, 4), frandom(0,2));
			TNT1 A 1050 Bright A_BarrelDestroy();
			TNT1 A 5 A_Respawn();
			Wait;
	}
}