class HODA_Spark : Actor
{
	override void BeginPlay()
	{
		A_ScaleVelocity(frandom(0.5, 7.0));
		A_SetRoll(random(0, 360));
		A_SetScale(Scale.X * frandom(0.35, 1.0));
	
		super.BeginPlay();
	}

	override void tick()
	{
		A_SpawnItemEx("SparkTrail");
		A_ScaleVelocity(0.98);
		
		super.tick();
	}

	Default
	{
		Projectile;
		
		Radius 1;
		Height 2;
		Speed 6;
		Scale 0.01;
		Renderstyle "Add";
		BounceType "DOOM";
		BounceCount 6;
		BounceFactor 0.2;
		Gravity 0.5;
		
		+Bright
		-NoGravity
		+ThruActors
		+DONTSPLASH
		+ROLLSPRITE
	}

	States
	{
		Spawn:
			SPRK A random(35, 700) NoDelay;
		Faded:
			SPRK A 1 A_FadeTo(0.0, 0.1, TRUE);
			Loop;
	}
}

class SparkTrail : Actor
{
	Default
	{
		Scale 0.01;
		Alpha 1.0;
		Renderstyle "Add";
		
		+NOINTERACTION;
		+BRIGHT
	}

	States
	{
		Spawn:
			SPRK A 1 NoDelay A_FadeTo(0.0, 0.075, TRUE);
			Loop;
	}
}

class HODA_PlasmaSpark : Actor 
{
	override void BeginPlay()
	{
		A_SetRoll(random(0, 360));
		A_SetScale(Scale.X * frandom(0.35, 1.0));
	
		super.BeginPlay();
	}

	Default
	{
		Scale 0.02;
		Renderstyle "Add";
		
		+BRIGHT
		+NOINTERACTION
		+ROLLSPRITE
	}

	States
	{
		Spawn:
			SPKB E random(35, 105) NoDelay;
		Faded:
			SPKB EF random(0, 2) A_FadeTo(0.0, 0.1, TRUE);
			Loop;
	}
}

class HODA_Ember : Actor
{
	Default
	{
		Projectile;
		
		Radius 1;
		Height 2;
		Speed 10;
		Scale 0.05;
		Renderstyle "Add";
		BounceType "DOOM";
		BounceFactor 0.35;
		BounceCount 6;
		Gravity 0.25;

		+Bright
		-NoGravity
		+ThruActors
		+DONTSPLASH
		+ROLLSPRITE
	}

	States
	{
		Spawn:
			BTPF C 0 NODELAY A_SetRoll(random(0, 360));
			BTPF CCCAAAAAA 1 A_SpawnItemEx("EmberTrail");
			BTPF CCCAAAAAA 1 A_SpawnItemEx("EmberTrail");
			BTPF CCCAAAAAA 1 A_SpawnItemEx("EmberTrail");
			BTPF CCCAAAAAA 2 A_SpawnItemEx("EmberTrail");
			BTPF BBDD 3;
		Fade:
		Death:
			BTPF D 1 A_FadeTo(0.0, 0.05, TRUE);
			Loop;
	}
}

class EmberTrail : Actor
{
	Default
	{
		Scale 0.05;
		Renderstyle "Add";
		
		+NOINTERACTION;
		+BRIGHT;
	}

	States
	{
		Spawn:
			BTPF ABD 5;
		Fade:
			BTPF D 1 
			{
				A_FadeTo(0.0, 0.15, TRUE);
				A_SetScale(Scale.X * 0.85);
			}
			Loop;
	}
}

class HODA_FlashEmber : HODA_Ember
{
	Default 
	{
		Scale 0.035;
	}

	States 
	{
		Spawn:
			BTPF C 1 NoDelay A_SetScale(Scale.X * frandom(0.65, 1.1));
		Flying:
			BTPF C 1 
			{
				A_FadeTo(0.0, 0.05, TRUE);
				A_ScaleVelocity(0.97);
			}
			Loop;
	}
}
