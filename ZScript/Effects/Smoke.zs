class HODA_ExplosionSmoke : Actor
{
	Default
	{
		Scale 0.85;
		Alpha 0.25;
		+NoInteraction
		+ForceXYBillboard
		+ROLLSPRITE
		+ROLLCENTER
		+DONTSPLASH
	}

	States
	{
		Spawn:
			SMOK A 1 NoDelay
			{
				A_FadeOut(0.0035);
				A_SetScale(SCale.X + 0.025);
				A_SetRoll(Roll + 0.85);
			}
			Loop;
	}
}

class HODA_DebrisSmoke : Actor
{
	Default
	{
		Scale 0.1;
		Alpha 0.30;
		+ForceXYBillboard
		+ROLLSPRITE
		+ROLLCENTER
		+DONTSPLASH
		Gravity 0.05;
	}

	States
	{
		Spawn:
			DUST A 1 NoDelay
			{
				A_FadeOut(0.0025);
				A_SetScale(SCale.X + 0.005);
				A_SetRoll(Roll + 3);
				A_ScaleVelocity(0.8);
			}
			Loop;
	}
}

class SmallDebrisSmoke : HODA_DebrisSmoke
{
	Default
	{
		Scale 0.12;
		Alpha 0.4;
	}
}

class HODA_GunSmoke : HODA_ExplosionSmoke 
{
	Default 
	{
		Scale 0.05;
		Alpha 0.075;
	}

	States 
	{
		Spawn:
			SMOK B 1 NoDelay
			{
				A_FadeTo(0.0, 0.0025, TRUE);
				A_SetScale(SCale.X + 0.0005);
				A_SetRoll(Roll + 2);
				A_ScaleVelocity(0.9);
			}
			Loop;
	}
}

class HODA_SataeSmoke : HODA_GunSmoke
{
	Default 
	{
		Scale 0.07;
		ALpha 0.35;
	}
}

class HODA_BigDebrisSmoke : HODA_DebrisSmoke
{
	Default
	{
		Scale 0.85;
		Alpha 0.40;
		Gravity 0.05;
	}

	States
	{
		Spawn:
			DUST A 1
			{
				A_FadeOut(0.0025);
				A_SetScale(SCale.X + 0.01);
				A_SetRoll(Roll + 1.5);
				A_ScaleVelocity(0.8);
			}
			Loop;
	}
}

class HODA_WoodDustSmol : Actor 
{
	Default 
	{
		
		Scale 0.05;
		Alpha 0.35;
		Gravity 0.01;
	
		RenderStyle "Translucent";
	
		+THRUACTORS;
		+NOCLIP;
		+ROLLSPRITE;
		+ROLLCENTER;
		+DONTSPLASH;
	}
	
	States 
	{
		Spawn:
			DUST B 1 NoDelay A_SetRoll(random(0, 360));
			DUST BBB 1 
			{
				A_SetScale(Scale.X * 1.5);
			}
			DUST B 3;
		DeathLoop:
			DUST B 1 A_FadeTo(0.0, 0.02, TRUE);
			Loop;
	}
}

class HODA_WoodDust : HODA_WoodDustSmol
{
	Default
	{
		Scale 0.35;
		Alpha 0.65;
	}
}

class HODA_DebrisTrail : Actor
{
	Default
	{
		Projectile;
		-NOGRAVITY;
		Height 6;
		Radius 3;
		Speed 30;
		Gravity 0.35;
		ReactionTime 15;
		+DONTSPLASH;
	}

	States
	{
		Spawn:
			TNT1 A 1 NoDelay
			{
				A_SpawnItemEx("SmallDebrisSmoke");
				A_CountDown();
			}
			Loop;
		Death:
			Stop;
	}
}

class HODA_TeleportFog : TeleportFog replaces TeleportFog 
{
	Default 
	{
		Alpha 0.85;
	}
	
	States 
	{
		Spawn:
			TFOG GFEDCBABCDEFGHIJ 1 Bright NoDelay;
			Stop;
	}
}