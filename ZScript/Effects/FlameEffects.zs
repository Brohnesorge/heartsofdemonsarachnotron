class HEExplosion : Actor
{
	Default
	{
		+NoInteraction
		Renderstyle "Add";
		Alpha 0.7;
		Scale 0.25;
	}

	States
	{
		Spawn:
			NDEX A 1 Bright NoDelay
			{
			
				if(HODA_Smoke) { A_SpawnItemEx("HODA_ExplosionSmoke", frandom(-16, 16), frandom(-16, 16), frandom(0, 16), 0, 0, random(0, 2)); }
				if(HODA_Embers)
				{
					for (int i = 0; i < random(0, 2); ++i)
						A_SpawnProjectile("HODA_Spark", 0, 0, frandom(0, 360), CMF_AIMDIRECTION, frandom(0, 360));
				}
			}
			NDEX BDF 1 Bright A_SetScale(Scale.X + 0.7);
			NDEX G 1 Bright
			{
				if (HODA_Smoke)
				{
					for (int i = 0; i < random(0, 1); ++i)
						A_SpawnItemEx("HODA_ExplosionSmoke", frandom(-48, 48), frandom(-48, 48), frandom(0, 32), 0, 0, random(0, 2));
				}
			}
			NDEX HIJKMNO 1 Bright A_FadeOut(0.05);
			Stop;
	}
}

class HEExplosionB : HEExplosion
{
	override void tick()
	{
		A_ScaleVelocity(0.9);
		A_SetScale(Scale.X + 0.1);

		super.Tick();
	}
	Default
	{
		Scale 1.0;
		Alpha 0.35;
		+ROLLSPRITE;
	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				A_SetScale(1.0, frandom(1.0, 1.5));
				bSpriteFlip = random(0, 1);
			}
			NDEX HIJKLMNO 2 Bright;
			Stop;
	}
}

class TinyExplosion : HEExplosionB
{
	override void tick()
	{
		A_ScaleVelocity(0.9);
		A_SetScale(Scale.X + 0.0015);
		A_FadeOut(0.03);

		super.Tick();
	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				A_SetScale(0.2, frandom(0.15, 0.35));
				A_SetRoll(frandom(-15, 15));
				bSpriteFlip = random(0, 1);
				if(HODA_Embers)
				{
					for (int i = 0; i < random(0, 3); ++i)
						A_SpawnProjectile("HODA_Spark", 0, 0, frandom(0, 360), CMF_AIMDIRECTION, frandom(-15, -75));
				}
			}
			NDEX IJKLMNO 1 Bright;
			Stop;
	}
}

class ExplosionTail : Actor 
{
	Default 
	{
		+NoInteraction;
		Renderstyle "Add";
		Alpha 0.65;
		Scale 0.35;
		
		+Bright;
	}
	States 
	{
		Spawn:
			NDEX GHIJKMNO 3;
			Stop;
	}
}

class ExplosionTailSpawn : Actor 
{
	override void tick()
	{
		A_SpawnItemEx("ExplosionTail");
		
		Super.Tick();
	}
	

	Default 
	{
		Projectile;
		Height 8;
		Radius 4;
		Speed 35;
		Renderstyle "Add";
		Scale 0.25;
		
		BounceType "Doom";
		BounceCount 9;
		BounceFactor 0.25;
		
		+BounceOnFloors;
		+THRUACTORS
		+NoClip;
		-NoGravity;
	}
	
	States
	{
		Spawn:
			TNT1 A 2; 
			TNT1 A 35 { bNoClip = FALSE; }
			Stop;
			
		Death:
			TNT1 A 0;
			Stop;
	}
}

class HODA_FireBase : Actor 
{
	override void BeginPlay()
	{
		A_SetScale(Scale.X * frandom(0.85, 1.15), Scale.Y * frandom(0.85, 1.15));
	
		super.BeginPlay();
	}

	Default 
	{
		Scale 1.0;
		Alpha 1.0;
		
		Renderstyle "Add";
	
		+BRIGHT;
		+NOINTERACTION;
		+FORCEXYBILLBOARD;
		+ROLLSPRITE;
		+ROLLCENTER;
	}
}

class HODA_FireTail : Actor 
{
	bool Alive;

	override void tick()
	{
		if(Alive)
		{
			A_SpawnItemEx("HODA_Fire");
		}
		
		Super.Tick();
	}
	

	Default 
	{
		Projectile;
		
		Height 8;
		Radius 4;
		Speed 20;
		
		Scale 0.25;
		
		Renderstyle "Add";
		
		BounceType "Doom";
		BounceCount 9;
		BounceFactor 0.25;
		
		+BOUNCEONFLOORS;
		+THRUACTORS
		+NOCLIP;
		-NOGRAVITY;
	}
	
	States
	{
		Spawn:
			TNT1 A 2 NoDelay { A_ScaleVelocity(frandom(0.5, 1.5)); self.Alive = TRUE; }
			TNT1 A 35 { self.bNOCLIP = FALSE; }
			Goto Death;
			
		Death:
			TNT1 A 0 { self.Alive = FALSE; }
			TNT1 AAA 2 A_SpawnItemEx("HODA_Fire", random(-12, 12), random(-12, 12), random(0, 8));
			TNT1 AAAA 4 A_SpawnItemEx("HODA_Fire", random(-6, 6), random(-6, 6), random(0, 4));
			TNT1 AAAAAA 6 A_SpawnItemEx("HODA_Fire");
			Stop;
	}
}

class HODA_Fire : HODA_FireBase
{
	override void PostBeginPlay()
	{
		A_SetRoll(self.roll + frandom(-25, 25));
		bSPRITEFLIP = random(0, 1);
	
		super.PostBeginPlay();
	}

	Default 
	{
		Scale 0.50;
		Alpha 0.25;
	}
	
	States 
	{
		Spawn:
			VFXF ABCDE 2 NoDelay;
			VFXF F 2 A_ChangeVelocity(0, 0, frandom(0.33, 1));
			VFXF GHI 2;
			Stop;
	}
}

class HODA_FireBlue : HODA_Fire 
{
	States 
	{
		Spawn:
			VFBF ABCDE 2 NoDelay;
			VFBF F 2 A_ChangeVelocity(0, 0, frandom(0.33, 1));
			VFBF GHI 2;
			Stop;
	}
}

class HODA_FireGreen : HODA_Fire 
{
	States 
	{
		Spawn:
			VFGF ABCDE 2 NoDelay;
			VFGF F 2 A_ChangeVelocity(0, 0, frandom(0.33, 1));
			VFGF GHI 2;
			Stop;
	}
}

class HODA_Flame : HODA_FireBase
{
	override void PostBeginPlay()
	{
		A_SetRoll(self.roll + random(-25, 25));
		bSPRITEFLIP = random(0, 1);
	
		super.PostBeginPlay();
	}

	Default
	{
		Scale 0.65;
		Alpha 0.40;
	}
	
	States 
	{
		Spawn:
			VFXA D 0 NoDelay;
			VFXA D 0 A_Jump(256, "Brun");
			Loop;
		
		Brun:
			#### B 2 
			{
				A_SetScale(Scale.X * frandom(0.8, 1.2), scale.y * frandom(0.8, 1.2));
				self.Alpha = Alpha * frandom(0.7, 1.3);
			}
			#### C 2;
			#### D 2 A_ChangeVelocity(0, 0, frandom(0.33, 1.33));
			#### DEFGHI 2;
			#### KLMNOPQ 1 A_FadeTo(0.0, 0.07, FALSE);
			Stop;
	}
}

class HODA_FireWave : Actor 
{
	Default 
	{
		Alpha 0.65;
		Scale 0.75;
		
		Renderstyle "Add";
		
		+NOINTERACTION;
		+BRIGHT;
	}

	States 
	{
		Spawn:
			VFXW AAAA 1 NoDelay A_SetScale(scale.x * 1.15);
			VFXW AAAA 1 { A_SetScale(scale.x * 1.15); A_FadeTo(0.0, 0.09, TRUE); }
			Stop;
	}
}

class HODA_FlameBlue : HODA_Flame
{
	States 
	{
		Spawn:
			VBXA D 0 NoDelay;
			VBXA D 0 A_Jump(256, "Brun");
			Loop;
	}
}

class HODA_FlameGreen : HODA_Flame
{
	States 
	{
		Spawn:
			VGXA D 0 NoDelay;
			VGXA D 0 A_Jump(256, "Brun");
			Loop;
	}
}
