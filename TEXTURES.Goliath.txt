// Texture definitions generated by SLADE3
// on Wed Oct  6 18:38:57 2021

Sprite "2CGGA0", 122, 82
{
	Offset -38, -115
	Patch "HCGGA0", 0, 0
}

Sprite "2CGGB0", 122, 82
{
	Offset -38, -115
	Patch "HCGGB0", 0, 0
}
 
Sprite "2CGGC0", 122, 82
{
	Offset -38, -115
	Patch "HCGGC0", 0, 0
}

Sprite "2CGGD0", 122, 82
{
	Offset -38, -115
	Patch "HCGGD0", 0, 0
}

Sprite "2CGFA0", 122, 114
{
	Offset -38, -95
	Patch "HCGFA0", 0, 0
}

Sprite "3CGFA0", 122, 119
{
	Offset -160, -95
	Patch "HCGFA0", 0, 0
}

Sprite "2CGFB0", 122, 106
{
	Offset -38, -100
	Patch "HCGFF0", 0, 0
}

Sprite "3CGFB0", 122, 111
{
	Offset -160, -100
	Patch "HCGFF0", 0, 0
}

Sprite "3CGGA0", 122, 82
{
	Offset -160, -115
	Patch "HCGGA0", 0, 0
}

Sprite "3CGGB0", 122, 82
{
	Offset -160, -115
	Patch "HCGGB0", 0, 0
}

Sprite "3CGGC0", 122, 82
{
	Offset -160, -115
	Patch "HCGGC0", 0, 0
}

Sprite "3CGGD0", 122, 82
{
	Offset -160, -115
	Patch "HCGGD0", 0, 0
}

// End of texture definitions
