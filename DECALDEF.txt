decal ArachnoBulletHole
{
	pic BulHole
	x-scale 0.25
	y-scale 0.25
	randomflipx
	randomflipy
}

decal GolHole
{
	pic AzaHole
	x-scale 0.35
	y-scale 0.35
	randomflipx
	randomflipy
}

decal SataeHole
{
	pic AzaHole
	x-scale 1.0
	y-scale 1.0
	randomflipx
	randomflipy
}

decal VinegaroonCrack
{
	pic VineCrak
	x-scale 1.0
	y-scale 1.0
	randomflipx
	randomflipy
}

decal PlasmaScorch
{
	pic BurnMark
	x-scale 0.2
	y-scale 0.2
	randomflipx
	randomflipy
}

decal WallBurn
{
	pic SCRCH2
	x-scale 0.75
	y-scale 0.75
	randomflipx
	randomflipy
	translucent 0.35
}


